#include "Grid.hpp"

#include <glm/vec3.hpp>

void Grid::Create(Vulkan::Device* device, const RenderInfo& renderInfo,const uint32_t& dimensions, const float spacing)
{
	std::vector<glm::vec3> vertices;

	vertices.push_back({ 0,0,0 });
	vertices.push_back({ 1,1,0 });
	const float halfWidth = static_cast<float>(dimensions) / 2.f * spacing;
	float pos = -halfWidth;
	for(uint32_t i=0;i <= dimensions;i++)
	{
		vertices.push_back({pos,0,-halfWidth});
		vertices.push_back({pos,0,halfWidth});
		vertices.push_back({-halfWidth,0,pos });
		vertices.push_back({halfWidth,0,pos});
		
		pos += spacing;
	}

	vertexBuffer.Create(device, vertices.size() * sizeof(glm::vec3));

	vertexBuffer.UploadWithStaging(vertices.data(), vertices.size() * sizeof(glm::vec3));

	count = vertices.size();


	Vulkan::ShaderModule gridVertShader, gridFragShader;

	gridVertShader.CreateFromFile(device, "../../Shaders/Compiled/Grid.vert.spv");
	gridFragShader.CreateFromFile(device, "../../Shaders/Compiled/Grid.frag.spv");


	Vulkan::VertexInput vertexInput;

	vertexInput.PushBinding(0, sizeof(glm::vec3), VK_VERTEX_INPUT_RATE_VERTEX);
	vertexInput.PushAttrib(0, 0, 0, VK_FORMAT_R32G32B32_SFLOAT);



	descriptorSet.Create(device, renderInfo.imgCount, {
		{0,Vulkan::DescriptorBinding::Uniform(sizeof(ViewDescriptor),renderInfo.viewUniformBuffers)}
		});

	pipeline.Create(device, vertexInput, descriptorSet, *renderInfo.renderPass, { 1280,720 }, {
		{gridVertShader,VK_SHADER_STAGE_VERTEX_BIT},
		{gridFragShader,VK_SHADER_STAGE_FRAGMENT_BIT}
		}, VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
}

void Grid::CmdDraw(const RenderInfo& info)
{
	descriptorSet.CmdBind(pipeline, info.currentImage);
	pipeline.CmdBind();
	vertexBuffer.CmdDraw(count);
	
}
