#include "Window.hpp"

#include <GLFW/glfw3.h>


 std::function<void(glm::vec2)> Window::cursorPosCallback;
 std::function<void(int, int, int, int)> Window::keyCallback;

void Window::Init()
{
	if(glfwInit() != GLFW_TRUE)
	{
		const char* message;
		glfwGetError(&message);
		throw Exception("Failed to init glfw: ", message);
	}
}

void Window::Destroy()
{
	glfwTerminate();
}


void Window::Create(const glm::uvec2& resolution, const char* title)
{
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	
	window = glfwCreateWindow(resolution.x, resolution.y, title, nullptr, nullptr);
	if(!window)
	{
		const char* message;
		glfwGetError(&message);
		throw Exception("Failed to create window: ",message);
	}

	glfwSetWindowUserPointer(window, this);

	glfwSetCursorPosCallback(window,GlfwCursorPosCallback);	
	glfwSetKeyCallback(window,GlfwKeyCallback);	
}

bool Window::IsOpen() const
{
	return !glfwWindowShouldClose(window);
}

void Window::PollEvents()
{
	glfwPollEvents();
}


std::vector<const char*> Window::GetInstanceExtensions()
{
	std::vector<const char*> result;
	uint32_t count;
	const char** extensions = glfwGetRequiredInstanceExtensions(&count);
	result.resize(count);
	std::memcpy(result.data(), extensions, sizeof(char*) * count);
	return std::move(result);
}

GLFWwindow* Window::GetHandle()
{
	return window;
}

void Window::SetCursorPosCallback(std::function<void(glm::vec2)> callback)
{
	cursorPosCallback = callback;
}

void Window::SetKeyCallback(std::function<void(int,int,int,int)> callback)
{
	keyCallback = callback;
}

void Window::SetCursorPos(const glm::vec2& pos)
{
	glfwSetCursorPos(window, pos.x, pos.y);
}

bool Window::IsLeftMouseButtonPressed() const
{
	return glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS;
}

void Window::GlfwCursorPosCallback(struct GLFWwindow* window, double xpos, double ypos)
{
	cursorPosCallback({xpos, ypos});
}

void Window::GlfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	keyCallback(key,scancode,action,mods);
}
