#pragma once

#include "Vulkan/VertexBuffer.hpp"
#include "Vulkan/Pipeline.hpp"
#include "Vulkan/CubeMap.hpp"
#include "RenderInfo.hpp"
#include <glm/vec2.hpp>

class Sky
{
	Vulkan::VertexBuffer vertexBuffer;
	Vulkan::Pipeline pipeline;
	uint32_t count = 0;

	Vulkan::DescriptorSet descriptor;

public:
	Vulkan::CubeMap cubeMap;
	void Create(Vulkan::Device* device, const RenderInfo& renderInfo);
	void CmdDraw(const RenderInfo& i);
};