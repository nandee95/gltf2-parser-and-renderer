#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>

#define PROPERTY(FUNC_POSTFIX,VAR) [[nodiscard]] const decltype(VAR)& Get ## FUNC_POSTFIX () const { return VAR; } \
									void Set ## FUNC_POSTFIX(const decltype(VAR)& value) { VAR =  value; }

class Camera
{
	glm::vec3 position{0,0,0};
	glm::quat orientation{ 1,0,0,0 };
	float fov = glm::radians(90.f);
public:
	PROPERTY(Position, position)
	PROPERTY(Orientation, orientation)
	PROPERTY(FOV, fov);

	const void Turn(glm::vec3 angle);

	const void Move(glm::vec3 direction);

	const glm::highp_mat4 CalcViewMatrix() const;

	const glm::mat4 CalcProjMatrix(const float& aspectRatio, const float& nearPlane = 1.f, const float& farPlane = 1e6) const;
};
