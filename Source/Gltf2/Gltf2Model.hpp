#pragma once

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>

#include "Exception.hpp"

#include <string>
#include <fstream>
#include <vector>
#include <limits>
#include <filesystem>

#include "Vulkan/VertexBuffer.hpp"
#include "Vulkan/IndexBuffer.hpp"
#include "Vulkan/Pipeline.hpp"
#include "Vulkan/Device.hpp"
#include "Vulkan/Texture.hpp"

namespace Gltf2
{
	namespace json = rapidjson;

	

	class Model
	{
		//Vulkan
		struct VkmdPrimitive
		{
			Vulkan::VertexBuffer posBuffer;
			uint32_t posBufferLen = 0;
			Vulkan::VertexBuffer normBuffer;
			uint32_t normBufferLen = 0;
			Vulkan::VertexBuffer uvBuffer;
			uint32_t uvBufferLen = 0;
			Vulkan::IndexBuffer indexBuffer;
			uint32_t indexLen = 0;
			uint32_t material;
		};

		struct VkmdMesh
		{
			std::vector<VkmdPrimitive> primitives;
		};

		struct VkmdMaterial
		{
			Vulkan::Texture texture;
		};

		std::vector<VkmdMesh> vkMeshes;
		std::vector<VkmdMaterial> vkMaterials;

		struct Buffer
		{
			std::string fileName;
			int32_t byteLength;
		};

		enum class AccessorType : uint32_t
		{
			SCALAR,
			VEC2,VEC3,VEC4,
			MAT2,MAT3,MAT4,
			__MAX
		};
		static constexpr const uint32_t accessorComponents[] = { 1,2,3,4,4,9,16 };

		enum class AccessorComponentType : uint32_t
		{
			BYTE = 5120,
			UNSIGNED_BYTE=5121,
			SHORT=5122,
			UNSIGNED_SHORT = 5123,
			UNSIGNED_INT = 5125,
			FLOAT = 5126
		};
		static constexpr const uint32_t accessorTypeLen[] = { 1,1,2,2,0,4,4 };

		enum class BufferViewTarget : int32_t
		{
			NO_TARGET = 0,
			ARRAY_BUFFER = 34962,
			ELEMENT_ARRAY_BUFFER = 34963
		};

		struct BufferView
		{
			int32_t buffer;
			int32_t byteOffset;
			int32_t byteStride;
			int32_t byteLength;
			BufferViewTarget target;
		};

		struct Accessor
		{
			int32_t bufferView;
			int32_t byteOffset;
			AccessorComponentType componentType;
			bool normalized;
			int32_t count;
			std::vector<float> max;
			std::vector<float> min;
			AccessorType type;
		};

		enum class PrimitiveMode : uint32_t
		{
			POINTS,
			LINES,
			LINE_LOOP,
			LINE_STRIP,
			TRIANGLES,
			TRIANGLE_STRIP,
			TRIANGLE_FAN
		};

		enum SamplerFilter
		{
			AUTO,
			NEAREST = 9728,
			LINEAR = 9729,
			NEAREST_MIPMAP_NEAREST = 9984,
			LINEAR_MIPMAP_NEAREST = 9985,
			NEAREST_MIPMAP_LINEAR = 9986,
			LINEAR_MIPMAP_LINEAR = 9987
		};

		enum SamplerWrap
		{
			CLAMP_TO_EDGE = 33071,
			MIRRORED_REPEAT = 33648,
			REPEAT = 10497
		};

		struct Sampler
		{
			SamplerFilter minFilter;
			SamplerFilter magFilter;
			SamplerWrap wrapS;
			SamplerWrap wrapT;
		};

		struct Attributes
		{
			int32_t NORMAL;
			int32_t POSITION;
			int32_t TEXCOORD_0;
		};

		struct Primitive
		{
			Attributes attributes;
			int32_t indices;
			PrimitiveMode mode;
			int32_t material;
		};

		struct Mesh
		{
			std::vector<Primitive> primitives;
			std::string name;
		};

		struct Node
		{
			glm::mat4 matrix;
			uint32_t mesh;
			std::vector<uint32_t> children;
		};

		struct Image
		{
			std::string fileName;
		};

		struct Texture
		{
			int32_t sampler;
			int32_t source;
		};

		struct PbrTexture
		{
			int32_t index;
			int32_t texCoord;
		};

		struct PbrMetallicRoughness
		{
			PbrTexture baseColorTexture;
			float metallicFactor;
			float roughnessFactor;
		};

		struct Material
		{
			PbrMetallicRoughness pbrMetallicRoughness;
		};

		struct Scene
		{
			std::vector<uint32_t> rootNodes;
		};

		uint32_t scene = -1;
		
		std::vector<Vulkan::VertexBuffer> vertexBuffers;
		Vulkan::Pipeline pipeline;
		Vulkan::DescriptorSet descriptorSet;

	public:

		void Load(Vulkan::Device* device,const RenderInfo& info, const char* filename,Sky& sky)
		{
			std::vector<Buffer> buffers;
			std::vector<BufferView> bufferViews;
			std::vector<Accessor> accessors;
			std::vector<Mesh> meshes;
			std::vector<Image> images;
			std::vector<Sampler> samplers;
			std::vector<Texture> textures;
			std::vector<Material> materials;
			std::vector<Node> nodes;
			std::vector<Scene> scenes;

			const std::string rootDir = std::string(filename).substr(0,std::string(filename).find_last_of('/')+1);
			// Parse gltf
			{

				json::Document glTF;
				std::ifstream jsonIfs(filename);
				if (!jsonIfs) return;

				json::IStreamWrapper wrapper(jsonIfs);
				glTF.ParseStream(wrapper);

				// Load buffers
				if (glTF.HasMember("buffers") && glTF["buffers"].IsArray())
				{
					int32_t bc = 0;
					for (const auto& b : glTF["buffers"].GetArray())
					{
						buffers.push_back({});
						Buffer& buffer = buffers.back();
						if (b.HasMember("uri") && b["uri"].IsString())
						{
							buffer.fileName = rootDir + b["uri"].GetString();
							std::ifstream ifs(buffer.fileName,std::ios::binary);
							if (!ifs.good())
								throw Exception("\"", buffer.fileName, "\" is not a valid file");
						}
						else
							throw Exception("buffer[", bc, "].uri is missing");

						if (b.HasMember("byteLength") && b["byteLength"].IsInt() && b["byteLength"].GetInt() > 0)
						{
							buffer.byteLength = b["byteLength"].GetInt();

							std::ifstream ifs(buffer.fileName.c_str(), std::ios::binary | std::ios::ate);
							
							if (ifs.tellg() != buffer.byteLength)
								throw Exception("buffer[", bc, "].byteLength doesn't match with file size");
						}
						else
							throw Exception("buffer[", bc, "].byteLength is missing");

						bc++;
					}
				}
				else
				{
					throw Exception("glTF.buffers is missing");
				}

				if (glTF.HasMember("bufferViews") && glTF["bufferViews"].IsArray())
				{
					uint32_t bvc = 0;
					for (const auto& bv : glTF["bufferViews"].GetArray())
					{
						bufferViews.push_back({});
						BufferView& bufferView = bufferViews.back();
						if (bv.HasMember("buffer") && bv["buffer"].IsInt())
						{
							const int32_t buffer = bv["buffer"].GetInt();
							if (buffer >= 0 && buffer < buffers.size())
								bufferView.buffer = buffer;
							else
								throw Exception("bufferView.buffer is out of range");
						}
						else throw Exception("bufferView[", bvc, "].buffer is missing");

						if (bv.HasMember("byteOffset") && bv["byteOffset"].IsInt())
						{
							bufferView.byteOffset = bv["byteOffset"].GetInt();
							if (bufferView.byteOffset < 0 && bufferView.byteOffset >= buffers[bufferView.buffer].byteLength)
								throw Exception("bufferView[", bvc, "].byteOffset is out of range");
						}
						else bufferView.byteOffset = 0;

						if (bv.HasMember("byteLength") && bv["byteLength"].IsInt())
						{
							bufferView.byteLength = bv["byteLength"].GetInt();
							if (bufferView.byteLength < 0 && bufferView.byteLength >= buffers[bufferView.buffer].byteLength - bufferView.byteOffset)
								throw Exception("bufferView[", bvc, "].byteLength is out of range");
						}
						else throw Exception("bufferView[", bvc, "].byteLength is missing");

						if (bv.HasMember("byteStride") && bv["byteStride"].IsInt())
						{
							bufferView.byteStride = bv["byteStride"].GetInt();
							if (bufferView.byteStride < 4 || bufferView.byteStride > 252)
								throw Exception("bufferView[", bvc, "].byteStride is out of range");
						}
						else bufferView.byteStride = -1; // Tightly packed

						if (bv.HasMember("target") && bv["target"].IsInt())
						{
							bufferView.target = static_cast<BufferViewTarget>(bv["target"].GetInt());

							if (bufferView.target != BufferViewTarget::ARRAY_BUFFER && bufferView.target != BufferViewTarget::ELEMENT_ARRAY_BUFFER)
								throw Exception("bufferView[", bvc, "].target invalid enum");
						}
						else bufferView.target = BufferViewTarget::NO_TARGET;

						bvc++;
					}
				}

				if (glTF.HasMember("accessors") && glTF["accessors"].IsArray())
				{
					uint32_t ac = 0;
					for (const auto& a : glTF["accessors"].GetArray())
					{
						accessors.push_back({});
						Accessor& accessor = accessors.back();
						if (a.HasMember("bufferView") && a["bufferView"].IsInt())
						{
							accessor.bufferView = a["bufferView"].GetInt();
							if (accessor.bufferView < 0 || accessor.bufferView >= bufferViews.size())
								throw Exception("accessor[", ac, "].bufferView is out of range");
						}
						else accessor.bufferView = -1; // Initialize with zeros (can be overridden by sparse property)

						if (a.HasMember("componentType") && a["componentType"].IsInt())
						{
							accessor.componentType = static_cast<AccessorComponentType>(a["componentType"].GetInt());
							if (accessor.componentType < AccessorComponentType::BYTE || accessor.componentType > AccessorComponentType::FLOAT || static_cast<int32_t>(accessor.componentType) == 5124)
								throw Exception("accessor[", ac, "].bufferView invalid enum");
						}
						else throw Exception("accessor[", ac, "].componentType is missing");

						if (a.HasMember("byteOffset") && a["byteOffset"].IsInt())
						{
							accessor.byteOffset = a["byteOffset"].GetInt();

							if (accessor.byteOffset < 0 || bufferViews[accessor.bufferView].byteOffset + accessor.byteOffset > buffers[bufferViews[accessor.bufferView].buffer].byteLength)
								throw Exception("accessor[", ac, "].byteOffset is out of range");
						}
						else accessor.byteOffset = 0;

						if (a.HasMember("normalized") && a["normalized"].IsBool())
						{
							accessor.normalized = a["normalized"].GetBool();
						}
						else accessor.normalized = false;

						if (a.HasMember("count") && a["count"].IsInt())
						{
							accessor.count = a["count"].GetInt();
							if (accessor.count <= 0)
								throw Exception("accessor[", ac, "].count is out of range");
						}
						else throw Exception("accessor[", ac, "].count is missing");


						if (a.HasMember("type") && a["type"].IsString())
						{
							accessor.type = StringToAccessorType(ac, a["type"].GetString());
						}
						else throw Exception("accessor[", ac, "].type is missing");

						if (a.HasMember("max") && a["max"].IsArray())
						{
							if (a["max"].GetArray().Size() != accessorComponents[static_cast<uint32_t>(accessor.type)])
								throw Exception("accessor[", ac, "].max array size doesnt match components");

							uint32_t mc = 0;
							for (const auto& m : a["max"].GetArray())
							{
								if (accessor.componentType == AccessorComponentType::FLOAT && m.IsFloat())
								{
									accessor.max.push_back(m.GetFloat());
									mc++;
									continue;
								}
								if (!m.IsInt()) throw Exception("accessor[", ac, "].max[", mc, "] invalid type");

								accessor.max.push_back(m.GetInt());
								mc++;
							}
						}

						if (a.HasMember("min") && a["min"].IsArray())
						{
							if (a["min"].GetArray().Size() != accessorComponents[static_cast<uint32_t>(accessor.type)])
								throw Exception("accessor[", ac, "].min array size doesnt match components");

							uint32_t mc = 0;
							for (const auto& m : a["min"].GetArray())
							{
								if (accessor.componentType == AccessorComponentType::FLOAT && m.IsFloat())
								{
									accessor.min.push_back(m.GetFloat());
									mc++;
									continue;
								}
								if (!m.IsInt()) throw Exception("accessor[", ac, "].min[", mc, "] invalid type");

								accessor.min.push_back(m.GetInt());
								mc++;
							}
						}
						ac++;
					}
				}

				if (glTF.HasMember("meshes") && glTF["meshes"].IsArray())
				{
					uint32_t mc = 0;
					for (const auto& m : glTF["meshes"].GetArray())
					{
						meshes.push_back({ });
						auto& mesh = meshes.back();
						if (m.HasMember("primitives") && m["primitives"].IsArray())
						{
							const auto& primitives = m["primitives"];
							uint32_t pc = 0;
							for (const auto& p : primitives.GetArray())
							{
								mesh.primitives.push_back({});
								auto& primitive = mesh.primitives.back();
								if (p.HasMember("attributes") && p["attributes"].IsObject())
								{
									const auto& attributes = p["attributes"];
									if (attributes.HasMember("POSITION") && attributes["POSITION"].IsInt())
									{
										primitive.attributes.POSITION = attributes["POSITION"].GetInt();
										if (primitive.attributes.POSITION < 0 || primitive.attributes.POSITION >= accessors.size())
											throw Exception("meshes[", mc, "].primitives[", pc, "].attributes.POSITION is out of range");
										if (accessors[primitive.attributes.POSITION].type != AccessorType::VEC3 || accessors[primitive.attributes.POSITION].componentType != AccessorComponentType::FLOAT)
											throw Exception("meshes[", mc, "].primitives[", pc, "].attributes.POSITION invalid accessor type");
									}
									else primitive.attributes.POSITION = -1;

									if (attributes.HasMember("NORMAL") && attributes["NORMAL"].IsInt())
									{
										primitive.attributes.NORMAL = attributes["NORMAL"].GetInt();
										if (primitive.attributes.NORMAL < 0 || primitive.attributes.NORMAL >= accessors.size())
											throw Exception("meshes[", mc, "].primitives[", pc, "].attributes.NORMAL is out of range");
										if (accessors[primitive.attributes.NORMAL].type != AccessorType::VEC3 || accessors[primitive.attributes.NORMAL].componentType != AccessorComponentType::FLOAT)
											throw Exception("meshes[", mc, "].primitives[", pc, "].attributes.NORMAL invalid accessor type");
									}
									else primitive.attributes.NORMAL = -1;

									//TODO: UNSIGNED_BYTE, UNSIGNED_SHORT support
									if (attributes.HasMember("TEXCOORD_0") && attributes["TEXCOORD_0"].IsInt())
									{
										primitive.attributes.TEXCOORD_0 = attributes["TEXCOORD_0"].GetInt();
										if (primitive.attributes.TEXCOORD_0 < 0 || primitive.attributes.TEXCOORD_0 >= accessors.size())
											throw Exception("meshes[", mc, "].primitives[", pc, "].attributes.TEXCOORD_0 is out of range");
										if (accessors[primitive.attributes.TEXCOORD_0].type != AccessorType::VEC2 || accessors[primitive.attributes.TEXCOORD_0].componentType != AccessorComponentType::FLOAT)
											throw Exception("meshes[", mc, "].primitives[", pc, "].attributes.TEXCOORD_0 invalid accessor type");

									}
								}

								if (p.HasMember("indices") && p["indices"].IsInt())
								{
									primitive.indices = p["indices"].GetInt();
									if (primitive.indices < 0 || primitive.indices >= accessors.size())
										throw Exception("meshes[", mc, "].primitives[", pc, "].indices is out of range");
									if (accessors[primitive.indices].componentType == AccessorComponentType::FLOAT || accessors[primitive.indices].type != AccessorType::SCALAR)
										throw Exception("meshes[", mc, "].primitives[", pc, "].indices invalid accessor type");
								}
								else
									primitive.indices = -1;

								if (p.HasMember("mode") && p["mode"].IsInt())
								{
									const int32_t mode = p["mode"].GetInt();
									if (mode < 0 || mode > static_cast<int32_t>(PrimitiveMode::TRIANGLE_FAN))
										throw Exception("meshes[", mc, "].primitives[", pc, "].indices is out of range");
									primitive.mode = static_cast<PrimitiveMode>(mode);
								}
								else
									primitive.mode = PrimitiveMode::TRIANGLES;
								pc++;
							}
						}
						else throw Exception("mesh[", mc, "].primitives is missing");
						mc++;
					}
				}

				// TODO: buffer + mime, base64
				if (glTF.HasMember("images") && glTF["images"].IsArray())
				{
					uint32_t ic = 0;

					for (const auto& i : glTF["images"].GetArray())
					{
						images.push_back({});
						auto& image = images.back();
						if (i.HasMember("uri") && i["uri"].IsString())
						{
							image.fileName = rootDir + i["uri"].GetString();

							std::ifstream ifs(image.fileName, std::ios::binary);
							if (!ifs.good())
								throw Exception("\"", i["uri"].GetString(), "\" is not a valid file");
						}
						else throw Exception("images[",ic,"].uri is missing");
						ic++;
					}
				}

				if (glTF.HasMember("samplers") && glTF["samplers"].IsArray())
				{
					samplers.push_back({});
					auto& sampler = samplers.back();
					uint32_t sc = 0;

					for (const auto& s : glTF["samplers"].GetArray())
					{
						if (s.HasMember("magFilter") && s["magFilter"].IsInt())
						{
							sampler.magFilter = static_cast<SamplerFilter>(s["magFilter"].GetInt());
							if (sampler.magFilter != SamplerFilter::LINEAR && sampler.magFilter != SamplerFilter::NEAREST)
								throw Exception("samplers[", sc, "].magFilter invalid enum");
						}
						else sampler.magFilter = SamplerFilter::AUTO;

						if (s.HasMember("minFilter") && s["minFilter"].IsInt())
						{
							sampler.minFilter = static_cast<SamplerFilter>(s["minFilter"].GetInt());
							if (sampler.minFilter != SamplerFilter::LINEAR && sampler.minFilter != SamplerFilter::NEAREST
								&& sampler.minFilter != SamplerFilter::NEAREST_MIPMAP_NEAREST && sampler.minFilter != SamplerFilter::LINEAR_MIPMAP_NEAREST
								&& sampler.minFilter != SamplerFilter::NEAREST_MIPMAP_LINEAR && sampler.minFilter != SamplerFilter::LINEAR_MIPMAP_LINEAR)
								throw Exception("samplers[", sc, "].minFilter invalid enum");
						}
						else sampler.minFilter = SamplerFilter::AUTO;

						if (s.HasMember("wrapS") && s["wrapS"].IsInt())
						{
							sampler.wrapS = static_cast<SamplerWrap>(s["wrapS"].GetInt());
							if (sampler.wrapS != SamplerWrap::CLAMP_TO_EDGE && sampler.wrapS != SamplerWrap::MIRRORED_REPEAT&& sampler.wrapS != SamplerWrap::REPEAT)
								throw Exception("samplers[", sc, "].wrapS invalid enum");
						}
						else sampler.wrapS = SamplerWrap::REPEAT;
						if (s.HasMember("wrapT") && s["wrapT"].IsInt())
						{
							sampler.wrapT = static_cast<SamplerWrap>(s["wrapT"].GetInt());
							if (sampler.wrapT != SamplerWrap::CLAMP_TO_EDGE && sampler.wrapT != SamplerWrap::MIRRORED_REPEAT&& sampler.wrapT != SamplerWrap::REPEAT)
								throw Exception("samplers[", sc, "].wrapT invalid enum");
						}
						else sampler.wrapT = SamplerWrap::REPEAT;

						sc++;
					}
				}

				if (glTF.HasMember("textures") && glTF["textures"].IsArray())
				{
					textures.push_back({});
					auto& texture = textures.back();
					uint32_t tc = 0;

					for (const auto& t : glTF["textures"].GetArray())
					{
						if (t.HasMember("sampler") && t["sampler"].IsInt())
						{
							texture.sampler = t["sampler"].GetInt();

							if (texture.sampler < 0 || texture.sampler >= samplers.size())
								throw Exception("textures[", tc, "].sampler is out of range");
						}
						else texture.sampler = -1; // auto filtering, repeat wrap

						if (t.HasMember("source") && t["source"].IsInt())
						{
							texture.source = t["source"].GetInt();

							if (texture.source < 0 || texture.source >= images.size())
								throw Exception("textures[", tc, "].source is out of range");
						}
						else // Specs states underfined behaviour if not defined.
							throw Exception("textures[", tc, "].source is missing");

						tc++;
					}
				}

				if (glTF.HasMember("materials") && glTF["materials"].IsArray())
				{
					for (auto& m : glTF["materials"].GetArray())
					{
						materials.push_back({});
						auto& material = materials.back();

						if (m.IsObject())
						{
							if (m.HasMember("pbrMetallicRoughness") && m["pbrMetallicRoughness"].IsObject())
							{
								auto& pbrMetallicRoughness = m["pbrMetallicRoughness"];
								if (pbrMetallicRoughness.HasMember("baseColorTexture") && pbrMetallicRoughness["baseColorTexture"].IsObject())
								{
									auto& baseColorTexture = pbrMetallicRoughness["baseColorTexture"];
									if (baseColorTexture.IsObject())
									{
										if (baseColorTexture.HasMember("index") && baseColorTexture["index"].IsInt())
										{
											material.pbrMetallicRoughness.baseColorTexture.index = baseColorTexture["index"].GetInt();
										}
									}
								}
							}
						}
					}
				}
			}

			for (const auto& mesh : meshes)
			{
				vkMeshes.emplace_back();
				auto& vkMesh = vkMeshes.back();
				for(const auto& prim : mesh.primitives)
				{
					vkMesh.primitives.emplace_back();
					auto& vkPrim = vkMesh.primitives.back();

					{
						auto bvid = accessors[prim.attributes.POSITION].bufferView;
						auto bid = bufferViews[bvid].buffer;
						std::ifstream ifs(buffers[bid].fileName, std::ios::binary);
						ifs.seekg(std::ios::beg + accessors[prim.attributes.POSITION].byteOffset + bufferViews[bvid].byteOffset);
						std::vector<uint8_t> bufferData(bufferViews[bvid].byteLength);
						ifs.read(reinterpret_cast<char*>(bufferData.data()), bufferData.size());
						vkPrim.posBuffer.Create(device, bufferViews[bvid].byteLength);
						vkPrim.posBuffer.UploadWithStaging(bufferData.data(), bufferData.size());
						vkPrim.posBufferLen = accessors[prim.attributes.POSITION].count;
					}

					{
						auto bvid = accessors[prim.attributes.NORMAL].bufferView;
						auto bid = bufferViews[bvid].buffer;
						std::ifstream ifs(buffers[bid].fileName, std::ios::binary);
						ifs.seekg(std::ios::beg + accessors[prim.attributes.NORMAL].byteOffset + bufferViews[bvid].byteOffset);
						std::vector<uint8_t> bufferData(bufferViews[bvid].byteLength);
						ifs.read(reinterpret_cast<char*>(bufferData.data()), bufferData.size());
						vkPrim.normBuffer.Create(device, bufferViews[bvid].byteLength);
						vkPrim.normBuffer.UploadWithStaging(bufferData.data(), bufferData.size());
						vkPrim.normBufferLen = accessors[prim.attributes.NORMAL].count;
					}

					{
						auto bvid = accessors[prim.attributes.TEXCOORD_0].bufferView;
						auto bid = bufferViews[bvid].buffer;
						std::ifstream ifs(buffers[bid].fileName, std::ios::binary);
						ifs.seekg(std::ios::beg + accessors[prim.attributes.TEXCOORD_0].byteOffset + bufferViews[bvid].byteOffset);
						std::vector<uint8_t> bufferData(bufferViews[bvid].byteLength);
						ifs.read(reinterpret_cast<char*>(bufferData.data()), bufferData.size());
						vkPrim.uvBuffer.Create(device, bufferViews[bvid].byteLength);
						vkPrim.uvBuffer.UploadWithStaging(bufferData.data(), bufferData.size());
						vkPrim.uvBufferLen = accessors[prim.attributes.TEXCOORD_0].count;
					}

					if (prim.indices != -1)
					{
						auto bvid = accessors[prim.indices].bufferView;
						auto bid = bufferViews[bvid].buffer;
						std::ifstream ifs(buffers[bid].fileName, std::ios::binary);
						ifs.seekg(std::ios::beg + accessors[prim.indices].byteOffset + bufferViews[bvid].byteOffset);
						std::vector<uint8_t> bufferData(bufferViews[bvid].byteLength);
						ifs.read(reinterpret_cast<char*>(bufferData.data()), bufferData.size());
						vkPrim.indexBuffer.Create(device, bufferViews[bvid].byteLength);
						vkPrim.indexBuffer.UploadWithStaging(bufferData.data(), bufferData.size());
						vkPrim.indexLen = accessors[prim.indices].count;
					}

					vkPrim.material = prim.material;
				}
			}

			if (textures.size() > 0)
			{
				for (const auto& mat : materials)
				{
					vkMaterials.emplace_back();
					auto& vkMaterial = vkMaterials.back();

					vkMaterial.texture.CreateFromFile(device, *info.graphicsPool, images[textures[mat.pbrMetallicRoughness.baseColorTexture.index].source].fileName.c_str());
				}
			}

			Vulkan::ShaderModule vertShader, fragShader;

			vertShader.CreateFromFile(device, "../../Shaders/Compiled/Model.vert.spv");
			fragShader.CreateFromFile(device, "../../Shaders/Compiled/Model.frag.spv");


			Vulkan::VertexInput vertexInput;

			vertexInput.PushBinding(0, sizeof(glm::vec3), VK_VERTEX_INPUT_RATE_VERTEX);
			vertexInput.PushBinding(1, sizeof(glm::vec3), VK_VERTEX_INPUT_RATE_VERTEX);
			vertexInput.PushBinding(2, sizeof(glm::vec2), VK_VERTEX_INPUT_RATE_VERTEX);
			vertexInput.PushAttrib(0, 0, 0, VK_FORMAT_R32G32B32_SFLOAT);
			vertexInput.PushAttrib(1, 1, 0, VK_FORMAT_R32G32B32_SFLOAT);
			vertexInput.PushAttrib(2, 2, 0, VK_FORMAT_R32G32_SFLOAT);

			if (vkMaterials.size() == 0)
			{
				vkMaterials.emplace_back();
				vkMaterials.back().texture.CreateFromFile(device,*info.graphicsPool,"../../Resources/Models/black.jpg");
			}
			descriptorSet.Create(device, info.imgCount, {
				{0,Vulkan::DescriptorBinding::Uniform(sizeof(ViewDescriptor),info.viewUniformBuffers)},
				{1,Vulkan::DescriptorBinding::Image(sky.cubeMap.image,sky.cubeMap.sampler)},
				{2,Vulkan::DescriptorBinding::Image(vkMaterials[0].texture.image,vkMaterials[0].texture.sampler)}
				});
			pipeline.Create(device, vertexInput, descriptorSet, *info.renderPass, { 1280,720 }, {
				{vertShader,VK_SHADER_STAGE_VERTEX_BIT},
				{fragShader,VK_SHADER_STAGE_FRAGMENT_BIT}
				}, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
		}

		void CmdDraw(const RenderInfo& info)
		{

			descriptorSet.CmdBind(pipeline, info.currentImage);
			pipeline.CmdBind();
			for (const auto& m : vkMeshes)
			{
				for (const auto& p : m.primitives)
				{
					p.indexBuffer.CmdDrawIndexed({ p.posBuffer ,p.normBuffer, p.uvBuffer }, p.indexLen, VK_INDEX_TYPE_UINT16);
				}
			}
		}

		~Model()
		{
			for (auto& m : vkMeshes)
			{
				for (auto& p : m.primitives)
				{
					p.posBuffer.Destroy();
					p.normBuffer.Destroy();
					p.uvBuffer.Destroy();
					p.indexBuffer.Destroy();
				}
			}
		}

		AccessorType StringToAccessorType(const uint32_t& accessorId,const std::string& type)
		{
			if (type == "SCALAR") return AccessorType::SCALAR;
			if (type == "VEC2") return AccessorType::VEC2;
			if (type == "VEC3") return AccessorType::VEC3;
			if (type == "VEC4") return AccessorType::VEC4;
			if (type == "MAT2") return AccessorType::MAT2;
			if (type == "MAT3") return AccessorType::MAT3;
			if (type == "MAT4") return AccessorType::MAT4;
			throw Exception("accessor[", accessorId,"].type invalid enum: \"",type,"\"");
		}
	};
}
