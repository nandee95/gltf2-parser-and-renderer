#pragma once

#include "Vulkan/VertexBuffer.hpp"
#include "Vulkan/Pipeline.hpp"
#include "RenderInfo.hpp"
#include <glm/vec2.hpp>

class Grid
{
	Vulkan::VertexBuffer vertexBuffer;
	Vulkan::Pipeline pipeline;
	Vulkan::DescriptorSet descriptorSet;
	uint32_t count = 0;
public:
	void Create(Vulkan::Device* device,const RenderInfo& renderInfo, const uint32_t& dimensions, const float spacing);
	void CmdDraw(const RenderInfo& info);
	Vulkan::Pipeline& GetPipeline() { return pipeline; }
};