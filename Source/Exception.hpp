#pragma once

#include <sstream>
#include <exception>

class Exception : public std::exception
{
	std::string message;
public:
	template<typename... Args>
	Exception(Args... args) 
	{
		std::stringstream ss;
		(ss << ... << args);
		message = ss.str();
	}

	[[nodiscard]] char const* what() const override
	{
		return message.c_str();
	}

};