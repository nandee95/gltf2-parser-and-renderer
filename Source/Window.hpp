#pragma once
#include "Exception.hpp"
#include <glm/vec2.hpp>
#include <functional>
#include <vector>

#include <GLFW/glfw3.h>
struct GLFWwindow;

class Window
{
	GLFWwindow* window=nullptr;

	static std::function<void(glm::vec2)> cursorPosCallback;
	static std::function<void(int,int,int,int)> keyCallback;
public:
	static void Init();
	static void Destroy();
	void Create(const glm::uvec2& resolution, const char* title);
	[[nodiscard]] bool IsOpen() const;
	void PollEvents();
	static std::vector<const char*> GetInstanceExtensions();
	[[nodiscard]] GLFWwindow* GetHandle();

	void SetCursorPosCallback(std::function<void(glm::vec2)> callback);
	void SetKeyCallback(std::function<void(int, int, int, int)> callback);

	void SetCursorPos(const glm::vec2& pos);

	bool IsLeftMouseButtonPressed() const;

private:

	static void GlfwCursorPosCallback(GLFWwindow* window, double xpos, double ypos);
	static void GlfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
};