#include "Sky.hpp"

#include <glm/vec3.hpp>
#include <array>

static float skyboxVertices[] = {
    // positions          
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f,  1.0f
};

void Sky::Create(Vulkan::Device* device, const RenderInfo& renderInfo)
{
    vertexBuffer.Create(device, sizeof(skyboxVertices));

    vertexBuffer.UploadWithStaging(skyboxVertices, sizeof(skyboxVertices));

    count = sizeof(skyboxVertices) / sizeof(float) / 3;


    Vulkan::ShaderModule SkyVertShader, SkyFragShader;

    SkyVertShader.CreateFromFile(device, "../../Shaders/Compiled/Sky.vert.spv");
    SkyFragShader.CreateFromFile(device, "../../Shaders/Compiled/Sky.frag.spv");


    Vulkan::VertexInput vertexInput;

    vertexInput.PushBinding(0, sizeof(glm::vec3), VK_VERTEX_INPUT_RATE_VERTEX);
    vertexInput.PushAttrib(0, 0, 0, VK_FORMAT_R32G32B32_SFLOAT);

    cubeMap.CreateFromFile(device, *renderInfo.graphicsPool, "../../Resources/SkyBox/");

    descriptor.Create(device, renderInfo.imgCount, {
        {0,Vulkan::DescriptorBinding::Uniform(sizeof(ViewDescriptor),renderInfo.viewUniformBuffers)},
        {1,Vulkan::DescriptorBinding::Image(cubeMap.image,cubeMap.sampler)},
        });

    pipeline.Create(device, vertexInput, descriptor, *renderInfo.renderPass, { 1280,720 }, {
        {SkyVertShader,VK_SHADER_STAGE_VERTEX_BIT},
        {SkyFragShader,VK_SHADER_STAGE_FRAGMENT_BIT}
        }, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
}

void Sky::CmdDraw(const RenderInfo& info)
{
    descriptor.CmdBind(pipeline, info.currentImage);
    pipeline.CmdBind();
    vertexBuffer.CmdDraw(count);
}
