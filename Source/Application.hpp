#pragma once

#include <cstdint>


#include "Window.hpp"

class Application
{
	Window window;
public:
	Application();
	int32_t Run();

private:
	int32_t RunInternal();
};