#include "Camera.hpp"


const void Camera::Turn(glm::vec3 angle)
{
	orientation = glm::rotate(glm::normalize(orientation), angle.x, glm::vec3(0, 0, 1));
	orientation = glm::rotate(glm::normalize(orientation), angle.y, glm::vec3(0, 1, 0));
	orientation = glm::rotate(glm::normalize(orientation), angle.z, glm::vec3(1, 0, 0));
}

const void Camera::Move(glm::vec3 direction)
{
	position += (glm::vec3(1, 0, 0) * orientation) * direction.x;
	position += (glm::vec3(0, 1, 0) * orientation) * direction.y;
	position += (glm::vec3(0, 0, 1) * orientation) * direction.z;
}

const glm::highp_mat4 Camera::CalcViewMatrix() const
{
	const glm::vec3 eye = position;
	return glm::lookAt(eye, eye + (glm::vec3(1, 0, 0) * orientation), (glm::vec3(0, 1, 0) * orientation));
}

const glm::mat4 Camera::CalcProjMatrix(const float& aspectRatio, const float& nearPlane, const float& farPlane) const
{
	auto proj = glm::perspective(fov, aspectRatio, nearPlane, farPlane);
	proj[1][1] *= -1;
	return proj;
}
