#pragma once

#include "Vulkan/Vulkan.hpp"
#include "Vulkan/Device.hpp"

namespace Vulkan
{


	class Sampler
	{
		Vulkan::Device* device = nullptr;
		VkSampler sampler = VK_NULL_HANDLE;
	public:
		Sampler()
		{

		}

		~Sampler()
		{
			Destroy();
		}

		void Create(Vulkan::Device* device, const VkFilter& minFilter = VK_FILTER_LINEAR, const VkFilter& magFilter = VK_FILTER_LINEAR, const VkSamplerAddressMode& addressMode = VK_SAMPLER_ADDRESS_MODE_REPEAT)
		{
			if (IsValid())
			{
				throw Exception("Vulkan Sampler already exists!");
			}

			this->device = device;

			VkSamplerCreateInfo samplerInfo{};
			samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
			samplerInfo.minFilter = minFilter;
			samplerInfo.magFilter = magFilter;
			samplerInfo.addressModeU = addressMode;
			samplerInfo.addressModeV = addressMode;
			samplerInfo.addressModeW = addressMode;
			samplerInfo.anisotropyEnable = VK_TRUE;
			samplerInfo.maxAnisotropy = 16.0f;
			samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
			samplerInfo.unnormalizedCoordinates = VK_FALSE;
			samplerInfo.compareEnable = VK_FALSE;
			samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
			samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

			VkResult result;

			if ((result = vkCreateSampler(*device, &samplerInfo, nullptr, &sampler)) != VK_SUCCESS)
			{
				throw Exception("vkCreateSampler returned ", result);
			}
		}

		void Destroy()
		{
			if (sampler != VK_NULL_HANDLE)
			{
				vkDestroySampler(*device, sampler, nullptr);

				sampler = VK_NULL_HANDLE;
			}

			device = nullptr;
		}

		bool IsValid() const noexcept
		{
			return sampler != VK_NULL_HANDLE;
		}

		operator bool() const noexcept
		{
			return IsValid();
		}

		operator VkSampler ()
		{
			return sampler;
		}
		operator const VkSampler () const
		{
			return sampler;
		}
	};
}