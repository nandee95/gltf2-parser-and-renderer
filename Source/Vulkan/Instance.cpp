#include "Vulkan/Instance.hpp"
#include "Vulkan/PhysicalDevice.hpp"

#include <algorithm>
#include <string>

namespace Vulkan
{
	Instance::Instance() noexcept
	{
	}

	Instance::Instance(Instance&& other) noexcept : instance(other.instance), debugMessenger(other.debugMessenger)
	{
		other.instance = VK_NULL_HANDLE;
		other.debugMessenger = VK_NULL_HANDLE;
	}

	Instance::Instance(const char* engineName, const uint32_t& engineVersion, const char* appName, const uint32_t& appVersion, const uint32_t& apiVersion, const std::vector<const char*>& layers, const std::vector<const char*>& extensions)
	{
		Create(engineName, engineVersion, appName, appVersion, apiVersion, layers, extensions);
	}

	Instance::~Instance() noexcept
	{
		Destroy();
	}

	void Instance::Create(const char* engineName, const uint32_t& engineVersion, const char* appName, const uint32_t& appVersion, const uint32_t& apiVersion, const std::vector<const char*>& layers, const std::vector<const char*>& extensions)
	{
		if (IsValid())
		{
			throw Exception( "Vulkan Instance already exists!");
		}

		VkApplicationInfo appInfo{};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = appName;
		appInfo.applicationVersion = appVersion;
		appInfo.pEngineName = engineName;
		appInfo.engineVersion = engineVersion;
		appInfo.apiVersion = apiVersion;

		VkInstanceCreateInfo instanceInfo{};
		instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceInfo.pApplicationInfo = &appInfo;
		instanceInfo.enabledLayerCount = static_cast<uint32_t>(layers.size());
		instanceInfo.ppEnabledLayerNames = layers.data();
		instanceInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		instanceInfo.ppEnabledExtensionNames = extensions.data();

		VkResult result;
		if ((result = vkCreateInstance(&instanceInfo, nullptr, &instance)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateInstance returned ", result);
		}
	}

	void Instance::Destroy() noexcept
	{
		if (debugMessenger != VK_NULL_HANDLE)
		{
			GetProcAddr<PFN_vkDestroyDebugUtilsMessengerEXT>("vkDestroyDebugUtilsMessengerEXT")(instance, debugMessenger, nullptr);
			debugMessenger = VK_NULL_HANDLE;
		}

		if (instance != VK_NULL_HANDLE)
		{
			vkDestroyInstance(instance, nullptr);
			instance = VK_NULL_HANDLE;
		}
	}

	bool Instance::IsValid() const noexcept
	{
		return instance != VK_NULL_HANDLE;
	}

	Instance::operator bool() const noexcept
	{
		return IsValid();
	}

	void Instance::CreateDebugMessenger(PFN_vkDebugUtilsMessengerCallbackEXT callback, void* userData, const MsgSeverity& severity, const MsgType& type)
	{
		VkDebugUtilsMessengerCreateInfoEXT debugInfo = {};
		debugInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		debugInfo.messageSeverity = static_cast<VkFlags>(severity);
		debugInfo.messageType = static_cast<VkFlags>(type);
		debugInfo.pfnUserCallback = callback;
		debugInfo.pUserData = userData;

		VkResult result;
		if ((result = GetProcAddr<PFN_vkCreateDebugUtilsMessengerEXT>("vkCreateDebugUtilsMessengerEXT")(instance, &debugInfo, nullptr, &debugMessenger)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateDebugUtilsMessengerEXT returned ", result);
		}
	}

	std::vector<PhysicalDevice> Instance::GetPhysicalDevices() noexcept
	{
		uint32_t count = 0;
		vkEnumeratePhysicalDevices(instance, &count, nullptr);
		if (count == 0)
			return {};
		std::vector<VkPhysicalDevice> vkPhysicalDevices(count);
		vkEnumeratePhysicalDevices(instance, &count, vkPhysicalDevices.data());
		std::vector<PhysicalDevice> physicalDevices(count);
		std::transform(vkPhysicalDevices.begin(), vkPhysicalDevices.end(), physicalDevices.begin(), [this](VkPhysicalDevice& pd) { return PhysicalDevice(this, pd); });
		return std::move(physicalDevices);
	}

	std::vector<VkExtensionProperties> Instance::GetSupportedExtensions() noexcept
	{
		uint32_t count;
		vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr);
		std::vector<VkExtensionProperties> extensions(count);
		vkEnumerateInstanceExtensionProperties(nullptr, &count, extensions.data());
		return extensions;
	}

	Instance::operator const VkInstance() const noexcept
	{
		return instance;
	}

	Instance::operator VkInstance() noexcept
	{
		return instance;
	}

	Instance& Instance::operator = (Instance&& other) noexcept
	{
		Destroy();
		instance = other.instance;
		debugMessenger = other.debugMessenger;
		other.instance = VK_NULL_HANDLE;
		other.debugMessenger = VK_NULL_HANDLE;
		return *this;
	}
}