#pragma once
#include "Vulkan/Vulkan.hpp"
#include "Vulkan/Buffer.hpp"

#include "Vulkan/CommandBuffer.hpp"

#include <map>

namespace Vulkan
{
	struct DescriptorBinding
	{
		enum class Type
		{
			Unknown,Uniform,Image
		} type;
		VkDescriptorType descriptorType;
		VkShaderStageFlagBits stageFlags;
		union
		{
			VkDescriptorBufferInfo bufferInfo;
			VkDescriptorImageInfo imageInfo;
		};
		std::vector<VkBuffer> buffer;

		static DescriptorBinding Image(const VkImageView& imageView, const VkSampler& sampler)
		{
			DescriptorBinding result{};
			result.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			result.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
			result.imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			result.imageInfo.imageView = imageView;
			result.imageInfo.sampler = sampler;
			result.type = Type::Image;
			return result;
		}
		static DescriptorBinding NullImage()
		{
			DescriptorBinding result{};
			result.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			return result;
		}
		static DescriptorBinding Uniform(const size_t& size, std::vector<VkBuffer> buffers)
		{
			DescriptorBinding result{};
			result.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			result.stageFlags = static_cast<VkShaderStageFlagBits>(VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
			result.bufferInfo.buffer = buffers[0];
			result.bufferInfo.range = size;
			result.buffer = buffers;
			result.type = Type::Uniform;
			return result;
		}
	};

	class Device;
	class DescriptorSet
	{
	private:
		VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
		VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
		std::vector<VkDescriptorSet> descriptorSets;
		Device * device;
	public:
		void Create(Device* device, const uint32_t& imgCount, std::map<uint32_t, DescriptorBinding> bindings);

		~DescriptorSet()
		{
			Destroy();
		}

		void Destroy();


		const VkDescriptorSet& Get(const uint32_t& i) const
		{
			return descriptorSets[i];
		}

		operator VkDescriptorSetLayout()
		{
			return descriptorSetLayout;
		}
		operator const VkDescriptorSetLayout() const
		{
			return descriptorSetLayout;
		}

		void CmdBind(const VkPipelineLayout& pipelineLayout, const uint32_t& currentImg, const VkPipelineBindPoint& bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS)
		{
			vkCmdBindDescriptorSets(*CommandBuffer::Active, bindPoint, pipelineLayout, 0, 1, &descriptorSets[currentImg], 0, nullptr);
		}

	};

}