#pragma once

#include "Vulkan/CommandBuffer.hpp"
#include "Vulkan/Buffer.hpp"

namespace Vulkan
{
	class VertexBuffer : protected Buffer
	{
	public:
		using Buffer::Destroy;
		using Buffer::operator VkBuffer&;
		using Buffer::operator const VkBuffer&;

		void Create(Device* device,const size_t& size)
		{
			Buffer::Create(device, size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
			Allocate(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		}

		void UploadWithStaging(const void* data, const uint32_t& size)
		{
			Buffer stagingBuffer;
			stagingBuffer.Create(device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
			stagingBuffer.Allocate(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
			stagingBuffer.UploadWithMapping(data, size);
			stagingBuffer.Copy(*this, size);
		}

		void CmdDraw(const uint32_t& vertexCount) const
		{
			const VkBuffer vertexBuffers[] = { buffer };
			const VkDeviceSize offsets[] = { 0 };
			vkCmdBindVertexBuffers(*CommandBuffer::Active, 0, 1, vertexBuffers, offsets);

			vkCmdDraw(*CommandBuffer::Active, static_cast<uint32_t>(vertexCount), 1, 0, 0);
		}
	};
}