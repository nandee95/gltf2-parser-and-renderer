#pragma once

#include "Vulkan/Sampler.hpp"
#include "Vulkan/Image.hpp"
#include "Vulkan/Buffer.hpp"
#include "Vulkan/CommandBuffer.hpp"

#include <turbojpeg.h>
#include <fstream>
#include <array>


namespace Vulkan
{
	class CubeMap
	{
		static inline tjhandle decompressHandle;
	public:
		Vulkan::Image image;
		Vulkan::Sampler sampler;
		static void Init()
		{
			decompressHandle = tjInitDecompress();
		}

		
		void CreateFromFile(Vulkan::Device* device,Vulkan::CommandPool& commandPool, const char* folder)
		{

			std::array<std::string, 6> faces
			{
				"right.jpg",
					"left.jpg",
					"top.jpg",
					"bottom.jpg",
					"front.jpg",
					"back.jpg"
			};

			int width, height, subsamp, colorspace;
			std::vector<uint8_t> rawBuf;
			uint32_t count=0;
			for(const auto& f : faces)
			{
				const std::string filename = folder+f;
				std::ifstream ifs(filename, std::ios::binary | std::ios::ate);

				if (ifs.bad())
				{
					throw Exception("Failed to open file to read: ", filename);
				}
				size_t size = ifs.tellg();
				ifs.seekg(std::ios::beg);
				size -= ifs.tellg();
				std::vector<uint8_t> compBuffer(size);
				ifs.read((char*)compBuffer.data(), compBuffer.size());

				if (tjDecompressHeader3(decompressHandle, compBuffer.data(), compBuffer.size(), &width, &height, &subsamp, &colorspace) != 0)
				{
					throw Exception("Failed to read jpeg header from file: ", filename);
				}

				if(count == 0)
					rawBuf.resize(width * height * 4 * 6);
				
				if (tjDecompress2(decompressHandle, compBuffer.data(), compBuffer.size(), rawBuf.data() + count * width * height * 4, width, width * 4, height, TJPF_RGBA, NULL) != 0)
				{
					throw Exception("Failed to decompress jpeg image: ", filename);
				}
				count++;
			}
			
			image.Create(device, { width,height }, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT| VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT,6);
			image.Allocate(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
			image.CreateImageView(VK_IMAGE_VIEW_TYPE_CUBE);

			VkImageSubresourceRange subresRange{};

			subresRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			subresRange.baseMipLevel = 0;
			subresRange.levelCount = 1;
			subresRange.baseArrayLayer = 0;
			subresRange.layerCount = 6;
			
			sampler.Create(device);


			Vulkan::Buffer stagingBuffer;
			stagingBuffer.Create(device, width*height*4*6, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
			stagingBuffer.Allocate(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

			
			stagingBuffer.UploadWithMapping(rawBuf.data(),rawBuf.size());


			
			Vulkan::CommandBuffer commandBuffer;
			commandBuffer.Allocate(&commandPool);

			commandBuffer.Begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);


			image.CmdTransitionLayout(0, VK_ACCESS_TRANSFER_WRITE_BIT,
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				subresRange
			);
			
			stagingBuffer.CmdCopyToImage(image, { 0,0 }, { static_cast<uint32_t>(width),static_cast<uint32_t>(height),1 },6);

			image.CmdTransitionLayout(VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
				subresRange
			);
			commandBuffer.End();

			device->GetGraphicsQueue().Submit(commandBuffer);
			device->GetGraphicsQueue().WaitIdle();


		}
		
	};
}