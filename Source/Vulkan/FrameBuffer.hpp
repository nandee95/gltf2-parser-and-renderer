#pragma once
#include "Vulkan/Vulkan.h"

#include <glm/vec2.hpp>
#include <vector>

namespace Vulkan
{
	class RenderPass;
	class FrameBuffer
	{
		RenderPass* renderPass=nullptr;
		VkFramebuffer frameBuffer = VK_NULL_HANDLE;
	public:
		FrameBuffer();
		FrameBuffer(RenderPass* renderPass, const glm::uvec2& resolution, const std::vector<VkImageView>& attachments);
		FrameBuffer(FrameBuffer& other) = delete;
		FrameBuffer(FrameBuffer&& other);

		~FrameBuffer();

		void Create(RenderPass* renderPass, const glm::uvec2& resolution, const std::vector<VkImageView>& attachments);
		void Destroy();

		bool IsValid() const;

		operator bool() const;

		FrameBuffer& operator = (FrameBuffer&& other);

		operator VkFramebuffer();
		operator const VkFramebuffer() const;
	};
}