
#if _WIN32
#define GLFW_EXPOSE_NATIVE_WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#endif

#include <glfw/glfw3.h>
#include <glfw/glfw3native.h>

#include "Vulkan/Surface.hpp"
#include "Vulkan/Instance.hpp"

namespace Vulkan
{
	Surface::Surface() noexcept
	{

	}

	Surface::Surface(Instance* instance, GLFWwindow* window)
	{
		Create(instance, window);
	}

	Surface::Surface(Surface&& other) noexcept : instance(other.instance), surface(other.surface)
	{
		other.instance = nullptr;
		other.surface = VK_NULL_HANDLE;
	}

	Surface::~Surface() noexcept
	{
		Destroy();
	}

	void Surface::Create(Instance* instance, GLFWwindow* window)
	{
		if (IsValid())
		{
			throw Exception( "Surface already exists");
		}

		this->instance = instance;

		VkWin32SurfaceCreateInfoKHR createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		createInfo.hwnd = glfwGetWin32Window(window);
		createInfo.hinstance = GetModuleHandle(nullptr);

		VkResult result;
		if ((result = vkCreateWin32SurfaceKHR(*instance, &createInfo, nullptr, &surface)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateWin32SurfaceKHR returned ",result);
		}
	}

	void Surface::Destroy() noexcept
	{
		if (surface != VK_NULL_HANDLE)
		{
			vkDestroySurfaceKHR(*instance, surface, nullptr);
			surface = VK_NULL_HANDLE;
		}
		instance = nullptr;
	}

	bool Surface::IsValid() const noexcept
	{
		return surface != VK_NULL_HANDLE;
	}
	
	Surface::operator bool() const noexcept
	{
		return IsValid();
	}

	Instance* Surface::GetInstance() noexcept
	{
		return instance;
	}

	Surface::operator const VkSurfaceKHR() const noexcept
	{
 		return surface;
	}

	Surface::operator VkSurfaceKHR() noexcept
	{
		return surface;
	}

	Surface& Surface::operator =(Surface&& other) noexcept
	{
		surface = other.surface;
		instance = other.instance;
		other.surface = VK_NULL_HANDLE;
		other.instance = nullptr;
		return *this;
	}
}

