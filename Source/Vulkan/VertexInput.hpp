#pragma once
#include "Vulkan/Vulkan.hpp"
#include <vector>

namespace Vulkan
{
	class VertexInput
	{
	protected:
		VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
		VkVertexInputBindingDescription bindingDescription{};
		std::vector<VkVertexInputBindingDescription> bindings;
		std::vector<VkVertexInputAttributeDescription> attribs;
	public:
		VertexInput() noexcept;

		void PushBinding(const uint32_t& binding, const uint32_t& stride, const VkVertexInputRate& inputRate) noexcept;
		void PushAttrib(const uint32_t& binding, const uint32_t& location, const uint32_t& offset, const VkFormat& format) noexcept;
		void SetInputRate(const VkVertexInputRate& inputRate) noexcept;

		operator const VkPipelineVertexInputStateCreateInfo* () const;
	};
}