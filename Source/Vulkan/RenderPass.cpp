#include "Vulkan/RenderPass.hpp"
#include "Vulkan/Device.hpp"
#include "Vulkan/FrameBuffer.hpp"
#include "Vulkan/CommandBuffer.hpp"

namespace Vulkan
{
	RenderPass::RenderPass(Device* device, const VkFormat& colorFormat, const VkFormat& depthFormat) : device(device)
	{
		Create(device, colorFormat, depthFormat);
	}

	void RenderPass::Create(Device* device, const VkFormat& colorFormat, const VkFormat& depthFormat)
	{
		if (IsValid())
		{
			throw Exception( "RenderPass::Create() : Vulkan render pass already exists!");
		}

		this->device = device;

		VkAttachmentDescription colorAttachment{};
		colorAttachment.format = colorFormat;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference colorAttachmentRef{};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentDescription depthAttachment{};
		VkAttachmentReference depthAttachmentRef{};

		depthAttachment.format =depthFormat;
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		depthAttachmentRef.attachment = 1;
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		
		VkSubpassDescription subpass{};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &colorAttachmentRef;
		if ( depthFormat != VK_FORMAT_UNDEFINED)
			subpass.pDepthStencilAttachment = &depthAttachmentRef;

		VkSubpassDependency dependency{};
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		std::vector<VkAttachmentDescription> attachments = { colorAttachment};
		if (depthFormat != VK_FORMAT_UNDEFINED) attachments.push_back(depthAttachment);

		VkRenderPassCreateInfo renderPassInfo{};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		renderPassInfo.pAttachments = attachments.data();
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;
		renderPassInfo.dependencyCount = 1;
		renderPassInfo.pDependencies = &dependency;

		VkResult result;
		if ((result = vkCreateRenderPass(*device, &renderPassInfo, nullptr, &renderPass)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateRenderPass returned ", result);
		}
	}

	RenderPass::RenderPass()
	{

	}

	RenderPass::RenderPass(RenderPass&& other) noexcept : device(other.device), renderPass(other.renderPass)
	{
		other.device = nullptr;
		other.renderPass = VK_NULL_HANDLE;
	}

	RenderPass::~RenderPass()
	{
		Destroy();
	}

	void RenderPass::Destroy()
	{
		if (renderPass != VK_NULL_HANDLE)
		{
			vkDestroyRenderPass(*device, renderPass, nullptr);
			renderPass = VK_NULL_HANDLE;
		}
		device = nullptr;
	}

	bool RenderPass::IsValid() const
	{
		return device != nullptr && renderPass != VK_NULL_HANDLE;
	}

	Device* RenderPass::GetDevice()
	{
		return device;
	}

	void RenderPass::CmdBegin(const FrameBuffer& frameBuffer, const VkRect2D& renderArea, const std::vector<VkClearValue>& clearValues)
	{
		VkRenderPassBeginInfo renderPassInfo{};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = renderPass;
		renderPassInfo.framebuffer = frameBuffer;
		renderPassInfo.renderArea = renderArea;
		renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassInfo.pClearValues = clearValues.data();

		vkCmdBeginRenderPass(*CommandBuffer::Active, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	}

	void RenderPass::CmdEnd()
	{
		vkCmdEndRenderPass(*CommandBuffer::Active);
	}

	RenderPass::operator VkRenderPass()
	{
		return renderPass;
	}

	RenderPass::operator const VkRenderPass() const
	{
		return renderPass;
	}

	RenderPass& RenderPass::operator=(RenderPass&& other) noexcept
	{
		device = other.device;
		renderPass = other.renderPass;
		other.device = nullptr;
		other.renderPass = VK_NULL_HANDLE;
		return *this;
	}
}
