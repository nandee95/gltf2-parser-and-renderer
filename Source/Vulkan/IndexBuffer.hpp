#pragma once
#include "Vulkan/VertexBuffer.hpp"

namespace Vulkan
{
	class IndexBuffer : protected Buffer
	{
	public:
		using Buffer::Destroy;

		void Create(Device* device, const size_t& size)
		{
			Buffer::Create(device, size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
			Allocate(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		}

		void UploadWithStaging(const void* data, const uint32_t& size)
		{
			Buffer stagingBuffer;
			stagingBuffer.Create(device, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
			stagingBuffer.Allocate(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
			stagingBuffer.UploadWithMapping(data, size);
			stagingBuffer.Copy(*this, size);
		}

		void CmdDrawIndexed(const std::vector<VkBuffer> vertexBuffers, const uint32_t& indexCount,const VkIndexType& indexType = VK_INDEX_TYPE_UINT32) const
		{
			std::vector<VkDeviceSize> offsets(vertexBuffers.size(),0);
			
			vkCmdBindVertexBuffers(*CommandBuffer::Active, 0, static_cast<uint32_t>(vertexBuffers.size()), vertexBuffers.data(), offsets.data());
			vkCmdBindIndexBuffer(*CommandBuffer::Active, buffer, 0, indexType);

			vkCmdDrawIndexed(*CommandBuffer::Active, indexCount, 1, 0, 0, 0);
		}
	};
}