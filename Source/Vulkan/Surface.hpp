#pragma once
#include "Vulkan/Vulkan.hpp"
#include "Exception.hpp"

struct GLFWwindow;
namespace Vulkan
{
	class Instance;

	/**
	* Vulkan Surface object.
	*/
	class Surface
	{
	protected:
		/**
		* Pointer to the parent Instance.
		*/
		Instance* instance=nullptr;
		/**
		* Vulkan surface object.
		*/
		VkSurfaceKHR surface=VK_NULL_HANDLE;
	public:
		/**
		* Empty constructor.
		*/
		Surface() noexcept;

		/**
		* Copy constructor. Copying a Surface is not allowed.
		*
		* @param other	Device to copy
		*/
		Surface(Surface&) = delete;

		/**
		* Move constructor. Moves the other Surface into the current Surface.
		*
		* @param other	Surface to move
		*/
		Surface(Surface&& other) noexcept;

		/**
		* @copydoc Vulkan::Surface::Create()
		*/
		Surface(Instance* instance, GLFWwindow* window);

		/**
		* Destroys the Surface.
		*/
		~Surface() noexcept;

		/**
		* Creates a surface.
		* 
		* @throw VulkanSurfaceAlreadyExists The Surface was valid
		* @throw VulkanFailedToCreateSurface Vulkan command failed
		* @param instance	parent Instance
		* @param window		window
		*/
		void Create(Instance* instance, GLFWwindow* window);

		/**
		* Destroys the Surface.
		*/
		void Destroy() noexcept;

		/**
		* Validates the Surface.
		*
		* @return	true if the Surface is valid
		*/
		bool IsValid() const noexcept;

		/**
		* @copydoc Vulkan::Surface::IsValid() 
		*/
		operator bool() const noexcept;
		/**
		* Returns the parent instance of the Surface.
		*
		* @return	Parent instance
		*/
		Instance* GetInstance() noexcept;

		/**
		* Returns the underlying VkSurfaceKHR.
		*
		* @return	VkSurfaceKHR object
		*/
		operator const VkSurfaceKHR() const noexcept;

		/**
		* Returns the underlying VkSurfaceKHR.
		*
		* @return	VkSurfaceKHR object
		*/
		operator VkSurfaceKHR() noexcept;

		/**
		* Move equal operator. Destroys the current Surface and moves the other Surface into itself.
		*
		* @param	other	Surface to move
		* @return	Reference to the moved Surface
		*/
		Surface& operator =(Surface&& other) noexcept;
	};
}