#include "Vulkan/Image.hpp"
#include "Vulkan/Device.hpp"
#include "Vulkan/Buffer.hpp"
#include "Vulkan/CommandBuffer.hpp"

namespace Vulkan
{
	Image::Image(
		Device* device,
		const glm::uvec2& resolution,
		const VkFormat& format,
		const VkImageTiling& tiling,
		const VkImageLayout& layout,
		const VkImageUsageFlags& usage,
		const VkImageCreateFlags& flags,
		const uint32_t& layers
	)
	{
		Create(device, resolution, format, tiling, layout, usage,flags,layers);
	}

	void Image::Create(
		Device* device,
		const glm::uvec2& resolution,
		const VkFormat& format,
		const VkImageTiling& tiling,
		const VkImageLayout& layout,
		const VkImageUsageFlags& usage,
		const VkImageCreateFlags& flags,
		const uint32_t& layers)
	{
		if (IsValid())
		{
			throw Exception( "Vulkan Image already exists");
		}

		this->device = device;
		this->format = format;
		this->layout = layout;
		this->resolution = resolution;
		VkImageCreateInfo imageInfo{};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent.width = resolution.x;
		imageInfo.extent.height = resolution.y;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = layers;
		imageInfo.format = format;
		imageInfo.tiling = tiling;
		imageInfo.initialLayout = layout;
		imageInfo.usage = usage;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageInfo.flags = flags;

		VkResult result;
		if ((result = vkCreateImage(*device, &imageInfo, nullptr, &image)) != VK_SUCCESS) {
			throw Exception( "vkCreateImage returned ", result);
		}
	}

	void Image::CreateImageView(const VkImageViewType& type)
	{
		VkImageViewCreateInfo viewInfo{};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image;
		viewInfo.viewType = type;
		viewInfo.format = format;
		viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

		VkResult result;
		if ((result = vkCreateImageView(*device, &viewInfo, nullptr, &imageView)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateImageView returned ", result);
		}
	}

	Image::Image(Image&& other) noexcept : image(other.image), imageView(other.imageView), device(other.device), format(other.format), layout(other.layout),resolution(other.resolution)
	{
		other.image = VK_NULL_HANDLE;
		other.imageView = VK_NULL_HANDLE;
		other.device = nullptr;
		other.format = VK_FORMAT_UNDEFINED;
		other.layout = VK_IMAGE_LAYOUT_UNDEFINED;
		other.resolution = { 0,0 };
	}
	void Image::Destroy()
	{
		if (imageView != VK_NULL_HANDLE)
		{
			vkDestroyImageView(*device, imageView, nullptr);
			imageView = VK_NULL_HANDLE;
		}

		Free();

		if (image != VK_NULL_HANDLE)
		{
			vkDestroyImage(*device, image, nullptr);
			image = VK_NULL_HANDLE;
		}
		device = nullptr;
		format = VK_FORMAT_UNDEFINED;
		layout = VK_IMAGE_LAYOUT_UNDEFINED;
		resolution = { 0,0 };
	}
	Image::~Image()
	{
		Destroy();
	}
	Image::operator VkImage()
	{
		return image;
	}
	Image::operator VkImageView()
	{
		return imageView;
	}
	Image::operator VkDeviceMemory()
	{
		return memory;
	}
	Image::operator const VkImage() const
	{
		return image;
	}
	Image::operator const VkImageView() const
	{
		return imageView;
	}
	Image::operator const VkDeviceMemory() const
	{
		return memory;
	}
	bool Image::IsValid() const
	{
		return image != VK_NULL_HANDLE;
	}
	Image::operator bool() const
	{
		return image != VK_NULL_HANDLE;
	}

	Image& Image::operator=(Image&& other) noexcept
	{
		image = other.image;
		imageView = other.imageView;
		device = other.device;
		format = other.format;
		layout = other.layout;
		resolution = other.resolution;
		other.image = VK_NULL_HANDLE;
		other.imageView = VK_NULL_HANDLE;
		other.device = nullptr;
		other.format = VK_FORMAT_UNDEFINED;
		other.layout = VK_IMAGE_LAYOUT_UNDEFINED;
		other.resolution = { 0,0 };
		return *this;
	}
	const VkFormat Image::GetFormat() const noexcept
	{
		return format;
	}

	void Image::CmdTransitionLayout(
		VkAccessFlags srcAccessMask,
		VkAccessFlags dstAccessMask,
		VkImageLayout oldImageLayout,
		VkImageLayout newImageLayout,
		VkPipelineStageFlags srcStageMask,
		VkPipelineStageFlags dstStageMask,
		VkImageSubresourceRange subresourceRange)
	{
		VkImageMemoryBarrier  barrier{};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.srcAccessMask = srcAccessMask;
		barrier.dstAccessMask = dstAccessMask;
		barrier.oldLayout = oldImageLayout;
		barrier.newLayout = newImageLayout;
		barrier.image = image;
		barrier.subresourceRange = subresourceRange;	

		vkCmdPipelineBarrier(
			*CommandBuffer::Active,
			srcStageMask, dstStageMask,
			0,
			0, nullptr,
			0, nullptr,
			1, &barrier
		);

	}

	void Image::CmdCopyToBuffer(Buffer& buffer, const VkImageAspectFlags& aspect)
	{
		VkBufferImageCopy imageCopy{};
		imageCopy.bufferImageHeight = static_cast<uint32_t>(resolution.y);
		imageCopy.bufferRowLength = static_cast<uint32_t>(resolution.x);
		imageCopy.imageExtent.width = static_cast<uint32_t>(resolution.x);
		imageCopy.imageExtent.height = static_cast<uint32_t>(resolution.y);
		imageCopy.imageExtent.depth = 1;
		imageCopy.imageSubresource.aspectMask = aspect;
		imageCopy.imageSubresource.layerCount = 1;
		vkCmdCopyImageToBuffer(*CommandBuffer::Active, image, layout, buffer, 1, &imageCopy);
	}


	void Image::CmdCopyToImage(Image& targetImage, const VkExtent3D& extent)
	{
		VkImageCopy imageCopy{};
		imageCopy.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageCopy.srcSubresource.layerCount = 1;
		imageCopy.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageCopy.dstSubresource.layerCount = 1;
		imageCopy.extent = extent;

		vkCmdCopyImage(
			*CommandBuffer::Active,
			image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			targetImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1,
			&imageCopy);
	}

	void Image::Allocate(const VkMemoryPropertyFlags& properties)
	{
		VkMemoryRequirements memRequirements;
		vkGetImageMemoryRequirements(*device, image, &memRequirements);

		VkMemoryAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = device->FindMemoryType(memRequirements.memoryTypeBits, properties);

		VkResult result;
		if (vkAllocateMemory(*device, &allocInfo, nullptr, &memory) != VK_SUCCESS)
		{
			throw Exception("vkAllocateMemory returned ",result);
		}

		if ((result = vkBindImageMemory(*device,image,memory,0)) != VK_SUCCESS)
		{
			throw Exception( "vmaBindImageMemory returned ", result);
		}
	}



	void Image::Free()
	{
		if (memory != VK_NULL_HANDLE)
		{
			vkFreeMemory(*device, memory,nullptr);
			memory = VK_NULL_HANDLE;
		}
	}

}