#include "Vulkan/FrameBuffer.hpp"
#include "Vulkan/RenderPass.hpp"
#include "Vulkan/Device.hpp"

namespace Vulkan
{
	FrameBuffer::FrameBuffer() {}
	FrameBuffer::FrameBuffer(RenderPass* renderPass, const glm::uvec2& resolution, const std::vector<VkImageView>& attachments) : renderPass(renderPass)
	{
		Create(renderPass, resolution, attachments);
	}

	void FrameBuffer::Create(RenderPass* renderPass, const glm::uvec2& resolution, const std::vector<VkImageView>& attachments)
	{
		if (IsValid())
		{
			throw Exception( "FrameBuffer already exists!");
		}
		this->renderPass = renderPass;

		VkFramebufferCreateInfo framebufferInfo{};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = *renderPass;
		framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferInfo.pAttachments = attachments.data();
		framebufferInfo.width = resolution.x;
		framebufferInfo.height = resolution.y;
		framebufferInfo.layers = 1;

		VkResult result;
		if ((result = vkCreateFramebuffer(*renderPass->GetDevice(), &framebufferInfo, nullptr, &frameBuffer)) != VK_SUCCESS) {
			throw Exception( "vkCreateFramebuffer returned ", result);
		}
	}


	FrameBuffer::operator bool() const
	{
		return IsValid();
	}

	FrameBuffer::FrameBuffer(FrameBuffer&& other) : renderPass(other.renderPass), frameBuffer(other.frameBuffer)
	{
		other.renderPass = nullptr;
		other.frameBuffer = VK_NULL_HANDLE;
	}
	FrameBuffer::~FrameBuffer()
	{
		Destroy();
	}
	void FrameBuffer::Destroy()
	{
		if (frameBuffer != VK_NULL_HANDLE)
		{
			vkDestroyFramebuffer(*renderPass->GetDevice(), frameBuffer, nullptr);
		}
		renderPass = nullptr;
	}

	bool FrameBuffer::IsValid() const
	{
		return frameBuffer != VK_NULL_HANDLE;
	}

	FrameBuffer& FrameBuffer::operator=(FrameBuffer&& other)
	{
		renderPass = other.renderPass;
		frameBuffer = other.frameBuffer;
		other.renderPass = nullptr;
		other.frameBuffer = VK_NULL_HANDLE;
		return *this;
	}



	FrameBuffer::operator VkFramebuffer()
	{
		return frameBuffer;
	}

	FrameBuffer::operator const VkFramebuffer() const
	{
		return frameBuffer;
	}
}