#pragma once
#include "Exception.hpp"
#include "Vulkan/Vulkan.hpp"

namespace Vulkan
{
	class Device;
	class ShaderModule
	{
	private:
		VkShaderModule shaderModule = VK_NULL_HANDLE;
		Device* device;
	public:
		~ShaderModule();

		void Create(Device* device, const void* data, const size_t& size);
		void CreateFromFile(Device* device, const char* filename);
		void Destroy();

		operator VkShaderModule();
		operator const VkShaderModule() const;
	};
}