#pragma once
#include "Vulkan/Vulkan.hpp"
#include "Vulkan/Queue.hpp"
#include <vector>

namespace Vulkan
{
	namespace DeviceExtension
	{
		constexpr const char* const Swapchain = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
	}

	class Instance;
	class Device;

	class PhysicalDevice
	{
	protected:
		Instance* instance = nullptr;
		VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
		friend Instance;
		PhysicalDevice(Instance* instance, const VkPhysicalDevice& physicalDevice);
	public:
		PhysicalDevice();

		Instance* GetInstance();

		operator const VkPhysicalDevice() const;
		operator VkPhysicalDevice();

		VkPhysicalDeviceProperties GetProperties() const noexcept;
		VkPhysicalDeviceProperties2 GetProperties2() const noexcept;
		VkSurfaceCapabilitiesKHR GetSurfaceCapabilities(const VkSurfaceKHR& surface) const noexcept;
		std::vector<VkSurfaceFormatKHR> GetSurfaceFormats(const VkSurfaceKHR& surface) const;
		std::vector<VkPresentModeKHR> GetSurfacePresentModes(const VkSurfaceKHR& surface) const;
		std::vector<VkQueueFamilyProperties> GetQueueFamilyProperties() const;
		std::vector<VkExtensionProperties> GetExtensions() const;

		VkSampleCountFlagBits GetMaxSampleCount() const;
	};
}