#include "Vulkan/CommandPool.hpp"
#include "Vulkan/Device.hpp"


namespace Vulkan
{
	CommandPool::CommandPool() noexcept
	{

	}

	CommandPool::CommandPool(CommandPool&& other) noexcept : device(other.device), commandPool(other.commandPool)
	{
		other.device = nullptr;
		other.commandPool = VK_NULL_HANDLE;
	}

	CommandPool::CommandPool(Device* device, const Queue& queue)
	{
		Create(device, queue);
	}

	CommandPool::~CommandPool() noexcept
	{
		Destroy();
	}

	void CommandPool::Create(Device* device, const Queue& queue)
	{
		if (IsValid())
		{
			throw Exception( "Vulkan CommandPool already exists");
		}

		this->device = device;
		VkCommandPoolCreateInfo commandPoolInfo{};
		commandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		commandPoolInfo.queueFamilyIndex = queue.GetFamilyIndex();

		VkResult result;
		if ((result = vkCreateCommandPool(*device, &commandPoolInfo, nullptr, &commandPool)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateCommandPool returned ", result);
		}
	}

	void CommandPool::Destroy() noexcept
	{
		if (commandPool != VK_NULL_HANDLE)
		{
			vkDestroyCommandPool(*device, commandPool, nullptr);
			commandPool = VK_NULL_HANDLE;
		}
		device = nullptr;
	}

	bool CommandPool::IsValid() const noexcept
	{
		return commandPool != VK_NULL_HANDLE;
	}

	CommandPool::operator bool() const noexcept
	{
		return IsValid();
	}

	Device* CommandPool::GetDevice() noexcept
	{
		return device;
	}

	CommandPool::operator VkCommandPool() noexcept
	{
		return commandPool;
	}

	CommandPool::operator const VkCommandPool() const noexcept
	{
		return commandPool;
	}

	CommandPool& CommandPool::operator=(CommandPool&& other) noexcept
	{
		Destroy();
		commandPool = other.commandPool;
		device = other.device;
		other.commandPool = VK_NULL_HANDLE;
		other.device = nullptr;
		return *this;
	}
};