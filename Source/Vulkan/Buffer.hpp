#pragma once
#include "Vulkan/Vulkan.hpp"
#include "Exception.hpp"

#include <glm/vec2.hpp>

namespace Vulkan
{
	class Device;
	class Image;
	class Buffer
	{
	protected:
		// Parent
		Device* device = nullptr;

		// Vulkan objects
		VkBuffer buffer = VK_NULL_HANDLE;
		VkDeviceMemory memory = VK_NULL_HANDLE;

		// Properties
		VkDeviceSize size = 0;

	public:
		Buffer();
		Buffer(Buffer& other) = delete;
		Buffer(Buffer&& other) noexcept;
		Buffer(Device* device, const VkDeviceSize& size, const VkBufferUsageFlags& usage);
		~Buffer();

		void Create(Device* device, const VkDeviceSize& size, const VkBufferUsageFlags& usage);
		void Destroy();
		bool IsValid() const;

		void Allocate(const VkMemoryPropertyFlags& flags);
		void Free();
		void Copy(Buffer& destination, const uint32_t& size);
		void UploadWithMapping(const void* data, const VkDeviceSize& size, const VkDeviceSize& offset=0);

		void CmdCopyToImage(Vulkan::Image& image, const VkOffset3D& offset, const VkExtent3D& extent, const uint32_t& layerCount=1);

		const VkDeviceSize& GetSize() const;

		Buffer& operator =(Buffer&& other) noexcept;
		operator const VkBuffer&() const;
		operator VkBuffer& ();
		operator const VkDeviceMemory&() const;
	};
}