#include "Vulkan/CommandBuffer.hpp"
#include "Vulkan/Device.hpp"

namespace Vulkan
{
	thread_local CommandBuffer* CommandBuffer::Active = nullptr;

	CommandBuffer::CommandBuffer() noexcept
	{

	}

	CommandBuffer::CommandBuffer(CommandBuffer&& other) noexcept : commandPool(other.commandPool), commandBuffer(other.commandBuffer)
	{
		other.commandPool = nullptr;
		other.commandBuffer = VK_NULL_HANDLE;
	}

	CommandBuffer::CommandBuffer(CommandPool* commandPool, const VkCommandBufferLevel& level) : commandPool(commandPool)
	{
		Allocate(commandPool, level);
	}

	CommandBuffer::~CommandBuffer() noexcept
	{
		Free();
	}

	void CommandBuffer::Allocate(CommandPool* commandPool, const VkCommandBufferLevel& level)
	{
		if (IsValid())
		{
			throw Exception( "Vulkan Command Buffer already exists");
		}

		this->commandPool = commandPool;
		VkCommandBufferAllocateInfo cmdBufferAllocInfo{};
		cmdBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		cmdBufferAllocInfo.commandPool = *commandPool;
		cmdBufferAllocInfo.commandBufferCount = 1;
		cmdBufferAllocInfo.level = level;

		VkResult result;
		if ((result = vkAllocateCommandBuffers(*commandPool->GetDevice(), &cmdBufferAllocInfo, &commandBuffer)) != VK_SUCCESS)
		{
			throw Exception( "vkAllocateCommandBuffers returned ", result);
		}
	}

	void CommandBuffer::Free() noexcept
	{
		if (commandBuffer != VK_NULL_HANDLE)
		{
			vkFreeCommandBuffers(*commandPool->GetDevice(), *commandPool, 1, &commandBuffer);
		}
		commandPool = nullptr;
	}

	void CommandBuffer::Begin(const VkCommandBufferUsageFlags& flags)
	{
		VkCommandBufferBeginInfo beginInfo{};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = flags;

		VkResult result;
		if ((result = vkBeginCommandBuffer(commandBuffer, &beginInfo)) != VK_SUCCESS)
		{
			throw Exception( "vkBeginCommandBuffer returned ", result);
		}
		Active = this;
	}

	void CommandBuffer::End()
	{
		VkResult result;
		if ((result = vkEndCommandBuffer(commandBuffer)) != VK_SUCCESS)
		{
			throw Exception( "vkEndCommandBuffer returned ", result);
		}
		Active = nullptr;
	}

	bool CommandBuffer::IsValid() const noexcept
	{
		return commandBuffer != VK_NULL_HANDLE;
	}

	CommandBuffer::operator bool() const noexcept
	{
		return IsValid();
	}

	CommandPool* CommandBuffer::GetCommandPool() noexcept
	{
		return commandPool;
	}

	CommandBuffer::operator const VkCommandBuffer& () const noexcept
	{
		return commandBuffer;
	}

	CommandBuffer::operator VkCommandBuffer& () noexcept
	{
		return commandBuffer;
	}

	CommandBuffer& CommandBuffer::operator=(CommandBuffer&& other) noexcept
	{
		commandPool = other.commandPool;
		commandBuffer = other.commandBuffer;
		other.commandPool = nullptr;
		other.commandBuffer = VK_NULL_HANDLE;
		return *this;
	}
}