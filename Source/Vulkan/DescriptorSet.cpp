#include "Vulkan/DescriptorSet.hpp"

#include <map>

#include "Vulkan/Device.hpp"

namespace Vulkan
{
	void DescriptorSet::Create(Device* device, const uint32_t& imgCount, std::map<uint32_t, DescriptorBinding> bindings)
	{
		this->device = device;

		// Bindings
		std::vector<VkDescriptorPoolSize> poolSizes;
		std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
		std::vector<VkWriteDescriptorSet> descriptorWrites;

		uint32_t buffers;
		for (const auto& binding : bindings)
		{
			VkDescriptorSetLayoutBinding layoutBinding{};
			layoutBinding.descriptorType = binding.second.descriptorType;
			layoutBinding.binding = binding.first;
			layoutBinding.descriptorCount = 1;
			layoutBinding.stageFlags = binding.second.stageFlags;			
			layoutBindings.push_back(layoutBinding);
			
			VkDescriptorPoolSize poolSize{};
			poolSize.type = binding.second.descriptorType;
			poolSize.descriptorCount = imgCount;
			poolSizes.push_back(poolSize);

			VkWriteDescriptorSet descriptorWrite{};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstBinding = binding.first;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.descriptorType = binding.second.descriptorType;
			if(binding.second.type == DescriptorBinding::Type::Image)
				descriptorWrite.pImageInfo = &binding.second.imageInfo;
			else
				descriptorWrite.pBufferInfo = &binding.second.bufferInfo;
			descriptorWrites.push_back(descriptorWrite);
		}
		
		// Create descriptor set layout
		VkDescriptorSetLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = static_cast<uint32_t>(layoutBindings.size());
		layoutInfo.pBindings = layoutBindings.data();

		VkResult result;
		if ((result = vkCreateDescriptorSetLayout(*device, &layoutInfo, nullptr, &descriptorSetLayout)) != VK_SUCCESS)
		{
			throw Exception("vkCreateDescriptorSetLayout returned ", result);
		}

	
		VkDescriptorPoolCreateInfo poolInfo{};
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.maxSets = imgCount;
		poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
		poolInfo.pPoolSizes = poolSizes.data();

		if ((result = vkCreateDescriptorPool(*device, &poolInfo, nullptr, &descriptorPool)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateDescriptorPool returned ", result);
		}

		std::vector<VkDescriptorSetLayout> setLayouts(imgCount, descriptorSetLayout);
		VkDescriptorSetAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = descriptorPool;
		allocInfo.descriptorSetCount = imgCount;
		allocInfo.pSetLayouts = setLayouts.data();

		descriptorSets.resize(imgCount, VK_NULL_HANDLE);
		
		if ((result = vkAllocateDescriptorSets(*device, &allocInfo, descriptorSets.data())) != VK_SUCCESS)
		{
			throw Exception( "vkAllocateDescriptorSets returned ", result);
		}

		for (uint32_t i = 0; i < imgCount; i++)
		{
			for(auto& dw : descriptorWrites)
				dw.dstSet = descriptorSets[i];

			vkUpdateDescriptorSets(*device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
		}
	}
	void DescriptorSet::Destroy()
	{
		if (descriptorSetLayout != VK_NULL_HANDLE)
		{
			vkDestroyDescriptorSetLayout(*device, descriptorSetLayout, nullptr);
			descriptorSetLayout = VK_NULL_HANDLE;
		}
		if (descriptorPool != VK_NULL_HANDLE)
		{
			vkDestroyDescriptorPool(*device, descriptorPool, nullptr);
			descriptorPool = VK_NULL_HANDLE;
		}
		descriptorSets.clear();

		device = nullptr;
	}
}