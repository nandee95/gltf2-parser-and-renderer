#pragma once
#include "Vulkan/Vulkan.hpp"
#include "Vulkan/Surface.hpp"

#include <vector>

namespace Vulkan
{
	class Device;

	struct SwapChainImage
	{
		VkImage image = VK_NULL_HANDLE;
		VkImageView imageView = VK_NULL_HANDLE;
	};

	class SwapChain
	{
	protected:

		VkSwapchainKHR swapchain = VK_NULL_HANDLE;
		Device* device = nullptr;
		std::vector<VkImage> images;
		std::vector<VkImageView> imageViews;

	public:

		void Create(Device* device, const Surface& surface, const uint32_t& imageCount, const VkFormat& format, const VkColorSpaceKHR& colorSpace, const VkExtent2D& extent,
			const VkSurfaceTransformFlagBitsKHR& preTransform, const VkPresentModeKHR& presentMode);

		SwapChainImage operator[](const uint32_t& id)
		{
			return { images[id] ,imageViews[id]};
		}

		uint32_t AcquireNextImage(const VkSemaphore& semaphore = VK_NULL_HANDLE, const VkFence& fence = VK_NULL_HANDLE, const uint64_t& timeOut = UINT64_MAX);

		void Destroy();

		~SwapChain()
		{
			Destroy();
		}

		operator const VkSwapchainKHR() const
		{
			return swapchain;
		}

	};
}