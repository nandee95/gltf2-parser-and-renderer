#pragma once
#include "Exception.hpp"

#include <vulkan/vulkan.h>

std::ostream& operator<<(std::ostream& e, VkResult const& result);

namespace Vulkan
{
	template<typename T>
	constexpr VkFormat TypeToFormat()
	{
		if constexpr (std::is_same<T, int8_t>::value)	return VK_FORMAT_R8_SINT;
		else if constexpr (std::is_same<T, uint8_t>::value)	return VK_FORMAT_R8_UINT;
		else if constexpr (std::is_same<T, int16_t>::value)	return VK_FORMAT_R16_SINT;
		else if constexpr (std::is_same<T, uint16_t>::value)	return VK_FORMAT_R16_UINT;
		else if constexpr (std::is_same<T, int32_t>::value)	return VK_FORMAT_R32_SINT;
		else if constexpr (std::is_same<T, uint32_t>::value)	return VK_FORMAT_R32_UINT;
		else if constexpr (std::is_same<T, int64_t>::value)	return VK_FORMAT_R64_SINT;
		else if constexpr (std::is_same<T, uint64_t>::value)	return VK_FORMAT_R64_UINT;
		else if constexpr (std::is_same<T, float>::value)	return VK_FORMAT_R32_SFLOAT;
		else if constexpr (std::is_same<T, double>::value)	return VK_FORMAT_R64_SFLOAT;
		else if constexpr (std::is_same<T, glm::vec2>::value)	return VK_FORMAT_R32G32_SFLOAT;
		else if constexpr (std::is_same<T, glm::ivec2>::value)	return VK_FORMAT_R32G32_SINT;
		else if constexpr (std::is_same<T, glm::uvec2>::value)	return VK_FORMAT_R32G32_UINT;
		else if constexpr (std::is_same<T, glm::vec3>::value)	return VK_FORMAT_R32G32B32_SFLOAT;
		else if constexpr (std::is_same<T, glm::ivec3>::value)	return VK_FORMAT_R32G32B32_SINT;
		else if constexpr (std::is_same<T, glm::uvec3>::value)	return VK_FORMAT_R32G32B32_UINT;
		else if constexpr (std::is_same<T, glm::vec4>::value)	return VK_FORMAT_R32G32B32A32_SFLOAT;
		else if constexpr (std::is_same<T, glm::ivec4>::value)	return VK_FORMAT_R32G32B32A32_SINT;
		else if constexpr (std::is_same<T, glm::uvec4>::value)	return VK_FORMAT_R32G32B32A32_UINT;
		static_assert("TypeToVkFormat() : Format not supported!");
	}
}