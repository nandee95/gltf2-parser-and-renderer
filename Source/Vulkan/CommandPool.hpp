#pragma once
#include "Exception.hpp"
#include "Vulkan/Vulkan.hpp"
#include "Vulkan/Queue.hpp"

namespace Vulkan
{
	class CommandPool
	{
	protected:
		// Parent
		Device* device = nullptr;

		// Vulkan object
		VkCommandPool commandPool = VK_NULL_HANDLE;
	public:
		/**
		* Empty constructor.
		*/
		CommandPool() noexcept;

		/**
		* Copy constructor. Copying a command pool is not allowed.
		* 
		* @param other	Command pool to copy
		*/
		CommandPool(CommandPool& other) = delete;

		/**
		* Move constructor. Moves the other command pool into the current command pool.
		* 
		* @param other	Command pool to move
		*/
		CommandPool(CommandPool&& other) noexcept;

		/**
		* Create constructor. Creates a command pool.
		* 
		* @param device	Valid Vulkan Device
		* @param queue	Target queue
		*/
		CommandPool(Device* device, const Queue& queue);

		/**
		* Destroys the command pool.
		*/
		~CommandPool() noexcept;

		/**
		* Creates a command pool.
		* Exceptions:
		*		VulkanCommandPoolAlreadyExists
		*		VulkanFailedToCreateCommandPool
		* 
		* @param device
		* @param queue
		*/
		void Create(Device* device, const Queue& queue);

		/**
		* Destroys the command pool.
		*/
		void Destroy() noexcept;

		/**
		* Validates the command pool.
		* 
		* @return	true if the command pool is valid
		*/
		bool IsValid() const noexcept;

		/**
		* Validates the command pool.
		*
		* @return	true if the command pool is valid
		*/
		operator bool() const noexcept;

		/**
		* Returns the parent devuce of the current command pool.
		* 
		* @return	Parent device
		*/
		Device* CommandPool::GetDevice() noexcept;

		/**
		* Returns the underlying VkCommandPool object.
		* 
		* @return	VkCommandPool command pool
		*/
		operator VkCommandPool() noexcept;

		/**
		* Returns the underlying VkCommandPool object.
		*
		* @return	VkCommandPool command pool
		*/
		operator const VkCommandPool() const noexcept;

		/**
		* Move equal operator. Destroys the current command pool and moves the other command pool into itself.
		* 
		* @param	other	Command pool to move
		* @return	Reference to this
		*/
		CommandPool& operator =(CommandPool&& other) noexcept;
	};
}