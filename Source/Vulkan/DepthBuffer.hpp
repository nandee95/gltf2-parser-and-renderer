#pragma once

#include <vulkan/vulkan.h>
#include <glm/vec2.hpp>

namespace Vulkan
{
	class Device;
	class CommandPool;

	class DepthBuffer
	{
	protected:
		VkImage image;
		VkDeviceMemory memory;
		VkImageView imageView;
		VkFormat format;
		Device* device;

	public:
		void Create(Device* device, CommandPool& commandPool, const glm::uvec2& resolution);

		DepthBuffer(DepthBuffer&& other) : image(other.image), memory(other.memory), imageView(other.imageView), device(other.device),format(other.format)
		{
			other.image = VK_NULL_HANDLE;
			other.imageView = VK_NULL_HANDLE;
			other.memory = VK_NULL_HANDLE;
			other.device = nullptr;
			other.format = VK_FORMAT_UNDEFINED;
		}

		DepthBuffer(DepthBuffer& other) = delete;

		DepthBuffer() : image(VK_NULL_HANDLE), memory(VK_NULL_HANDLE), imageView(VK_NULL_HANDLE), device(nullptr),format(VK_FORMAT_UNDEFINED)
		{

		}

		DepthBuffer& operator = (DepthBuffer&& other)
		{
			image = other.image;
			imageView = other.imageView;
			memory = other.memory;
			device = other.device;
			format = other.format;
			other.image = VK_NULL_HANDLE;
			other.imageView = VK_NULL_HANDLE;
			other.memory = VK_NULL_HANDLE;
			other.format = VK_FORMAT_UNDEFINED;
			other.device = nullptr;
			return *this;
		}

		~DepthBuffer()
		{
			Destroy();
		}

		void Destroy();

		operator VkImage()
		{
			return image;
		}

		operator const VkImage() const
		{
			return image;
		}

		operator VkImageView()
		{
			return imageView;
		}

		operator const VkImageView() const
		{
			return imageView;
		}

		const VkFormat& GetFormat() const
		{
			return format;
		}


	protected:
		void TransitionImageLayout(Device& device, CommandPool& cmdPool, VkImageLayout oldLayout, VkImageLayout newLayout);

		static bool FormatHasStencilComponent(VkFormat format);
	};
}