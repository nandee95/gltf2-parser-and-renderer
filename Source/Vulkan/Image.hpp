#pragma once
#include "Vulkan/Vulkan.hpp"
#include <glm/vec2.hpp>

namespace Vulkan
{
	class Device;
	class Buffer;
	class Image
	{
	protected:
		Device* device = nullptr;
		VkImage image = VK_NULL_HANDLE;
		VkImageView imageView = VK_NULL_HANDLE;
		VkDeviceMemory memory = VK_NULL_HANDLE;
		VkFormat format = VK_FORMAT_UNDEFINED;
		VkImageLayout layout = VK_IMAGE_LAYOUT_UNDEFINED;
		glm::vec2 resolution;
	public:
		Image(
			Device* device,
			const glm::uvec2& resolution,
			const VkFormat& format,
			const VkImageTiling& tiling,
			const VkImageLayout& layout,
			const VkImageUsageFlags& usage,
			const VkImageCreateFlags& flags=0,
			const uint32_t& layers=1
		);
		Image() {}
		Image(Image& other) = delete;
		Image(Image&& other) noexcept;
		~Image();


		void Create(
			Device* device,
			const glm::uvec2& resolution,
			const VkFormat& format,
			const VkImageTiling& tiling,
			const VkImageLayout& layout,
			const VkImageUsageFlags& usage,
			const VkImageCreateFlags& flags=0,
			const uint32_t& layers=1
		);
		void CreateImageView(const VkImageViewType& type= VK_IMAGE_VIEW_TYPE_2D);
		void Destroy();

		void Allocate(const VkMemoryPropertyFlags& properties);
		void Free();

		operator VkImage();
		operator VkImageView();
		operator VkDeviceMemory();
		operator const VkImage() const;
		operator const VkImageView() const;
		operator const VkDeviceMemory() const;

		bool IsValid() const;

		operator bool() const;

		Image& operator =(Image&& other) noexcept;

		const VkFormat GetFormat() const noexcept;

		void CmdTransitionLayout(
			VkAccessFlags srcAccessMask,
			VkAccessFlags dstAccessMask,
			VkImageLayout oldImageLayout,
			VkImageLayout newImageLayout,
			VkPipelineStageFlags srcStageMask,
			VkPipelineStageFlags dstStageMask,
			VkImageSubresourceRange subresourceRange);

		void CmdCopyToBuffer(Buffer& buffer, const VkImageAspectFlags& aspect = VK_IMAGE_ASPECT_COLOR_BIT);
		void CmdCopyToImage(Image& targetImage, const VkExtent3D& extent);
	};
}