#pragma once
#include "Vulkan/Vulkan.hpp"

#include <vector>

namespace Vulkan
{
	class PhysicalDevice;

	namespace InstanceLayer
	{
		/**
		* Shortcuts for the Instance layers.
		*/
		constexpr const char* const Validation = "VK_LAYER_KHRONOS_validation";
		constexpr const char* const Monitor = "VK_LAYER_LUNARG_monitor";
	}

	namespace InstanceExtension
	{
		/**
		* Shortcuts for the Instance extensions.
		*/
		constexpr const char* const DebugUtils = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
	}

	/**
	* Shortcuts for the message type flags.
	*/
	enum class MsgType
	{
		General = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
		Validation = VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
		Performance = VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
		All = General | Validation | Performance
	};

	/**
	* Shortcuts for the message severity flags.
	*/
	enum class MsgSeverity
	{
		Verbose = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT,
		Info = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
		Warning = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
		Error = VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
		All = Verbose | Info | Warning | Error,
		AllErrors = Warning | Error
	};

	class Instance
	{
	protected:
		// Vulkan objects
		VkInstance instance = VK_NULL_HANDLE;
		VkDebugUtilsMessengerEXT debugMessenger = VK_NULL_HANDLE;
	public:
		/**
		* Empy constructor.
		*/
		Instance() noexcept;
		/**
		* Copy constructor. Copying an Instance object is not allowed.
		* 
		* @param other Instance to copy
		*/
		Instance(Instance& other) = delete;

		/**
		* Move constructor.
		* 
		* @param other Instance to move
		*/
		Instance(Instance&& other) noexcept;

		/**
		* @copydoc Vulkan::Instance::Create()
		*/
		Instance(const char* engineName, const uint32_t& engineVersion, const char* appName, const uint32_t& appVersion, const uint32_t& apiVersion, const std::vector<const char*>& layers = {}, const std::vector<const char*>& extensions = {});
		
		/**
		* Destroys the Instance.
		*/
		~Instance() noexcept;

		/**
		* Creates an instance.
		* 
		* @throw	VulkanInstanceAlreadExists instance already exists
		* @throw	VulkanFailedToCreateInstance failed to create instance
		* @param engineName			Engine name
		* @param engineVersion		Engine version
		* @param appName			Application name
		* @param appVersion			Application version
		* @param apiVersion			Api version
		* @param layers				Instance layers
		* @param extensions			Instance extensions
		*/
		void Create(const char* engineName, const uint32_t& engineVersion, const char* appName, const uint32_t& appVersion, const uint32_t& apiVersion, const std::vector<const char*>& layers = {}, const std::vector<const char*>& extensions = {});
		
		/**
		* Destroys the Instance.
		* 
		* @return 
		*/
		void Destroy() noexcept;

		/**
		* Validates the Instance.
		* 
		* @return True if the instance is valid
		*/
		bool IsValid() const noexcept;
		
		/**
		* @copydoc Vulkan::Instance::IsValid()
		*/
		operator bool() const noexcept;

		/**
		* Initializes the debug messenger.
		* 
		* @throw VulkanFailedToCreateDebugMessenger		vkCreateDebugUtilsMessengerEXT failed
		* @param callback		callback function pointer
		* @param userData		user data
		* @param severity		message severity flags
		* @param type			message type flags
		*/
		void CreateDebugMessenger(PFN_vkDebugUtilsMessengerCallbackEXT callback, void* userData, const MsgSeverity& severity, const MsgType& type);

		/**
		* Gets a Vulkan procedure address.
		*
		* @throw VulkanGetProcAddressFailed failed to get process address
		* @tparam P function pointer type
		* @param name function name
		* @return function pointer
		*/
		template<typename P>
		P GetProcAddr(const char* name) noexcept
		{
			P proc = reinterpret_cast<P>(vkGetInstanceProcAddr(instance, name));
			if (!proc)
			{
				throw Exception( "vkGetInstanceProcAddr(\"", name, "\") returned nullptr");
			}
			return proc;
		}

		/**
		* Gets all Vulkan capable physical devices.
		* 
		* @return vector of physical devices
		*/
		std::vector<PhysicalDevice> GetPhysicalDevices() noexcept;

		/**
		* Gets all supported extensions.
		* 
		* @return vector of supported extensions
		*/
		static std::vector<VkExtensionProperties> GetSupportedExtensions() noexcept;

		/**
		* Returns the underlying VkInstance.
		* 
		* @return VkInstance object
		*/
		operator VkInstance() noexcept;

		/**
		* Returns the underlying VkInstance.
		*
		* @return VkInstance object
		*/
		operator const VkInstance() const noexcept;

		/**
		* Move equal operator. Destroys the current instance and moves the other instance into itself.
		* 
		* @param other Instance to move
		* @return Reference to the moved instance
		*/
		Instance& operator = (Instance&& other) noexcept;

	};
}