#include "Vulkan/Semaphore.hpp"
#include "Vulkan/Device.hpp"

namespace Vulkan
{
	void Semaphore::Create(Device* device, const VkFlags& flags)
	{
		this->device = device;
		if (semaphore != VK_NULL_HANDLE)
		{
			throw Exception( "Vulkan semaphore already exists!");
		}

		VkSemaphoreCreateInfo semaphoreInfo{};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		semaphoreInfo.flags = flags;

		VkResult result;
		if ((result = vkCreateSemaphore(*device, &semaphoreInfo, nullptr, &semaphore)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateSemaphore returned ", result);
		}

	}
	void Semaphore::Destroy()
	{
		if (semaphore != VK_NULL_HANDLE)
		{
			vkDestroySemaphore(*device, semaphore, nullptr);
		}
	}
}