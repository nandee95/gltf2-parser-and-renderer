#include "Vulkan/ShaderModule.hpp"
#include "Vulkan/Device.hpp"
#include <fstream>

namespace Vulkan
{
	void ShaderModule::Create(Device* device,const void* data, const size_t& size)
	{
		VkShaderModuleCreateInfo shaderModuleInfo{};
		shaderModuleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		shaderModuleInfo.pCode = reinterpret_cast<const uint32_t*>(data);
		shaderModuleInfo.codeSize = size;

		VkResult result;
		if ((result = vkCreateShaderModule(*device, &shaderModuleInfo, nullptr, &shaderModule)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateShaderModule returned ",result);
		}

		this->device = device;
	}

	void ShaderModule::CreateFromFile(Device* device, const char* filename)
	{
		std::ifstream ifs(filename, std::ios::binary | std::ios::ate);
		if (!ifs)
		{
			throw Exception( "Failed to open file: ", filename);
		}

		size_t size = ifs.tellg();
		ifs.seekg(std::ios::beg);
		size -= ifs.tellg();

		std::vector<uint8_t> buffer(size);
		ifs.read(reinterpret_cast<char*>(buffer.data()), buffer.size());

		Create(device,buffer.data(), buffer.size());
	}


	void ShaderModule::Destroy()
	{
		if (shaderModule != VK_NULL_HANDLE)
		{
			vkDestroyShaderModule(*device, shaderModule, nullptr);
			shaderModule = nullptr;
		}
		device = nullptr;
	}

	ShaderModule::operator VkShaderModule()
	{
		return shaderModule;
	}

	ShaderModule::operator const VkShaderModule() const
	{
		return shaderModule;
	}

	ShaderModule::~ShaderModule()
	{
		Destroy();
	}
}