#pragma once
#include "Vulkan/Vulkan.hpp"
#include "Vulkan/Semaphore.hpp"
#include "Vulkan/SwapChain.hpp"

#include <vector>

namespace Vulkan
{
	class Device;
	class Queue
	{
	protected:
		friend Device;
		VkQueue queue=VK_NULL_HANDLE;
		uint32_t familyIndex=-1;
	public:
		bool IsValid() const;
		operator bool();

		uint32_t GetFamilyIndex() const;

		void Submit(const VkCommandBuffer& commandBuffer, const VkSemaphore& waitSemaphore = VK_NULL_HANDLE, const VkSemaphore& signalSemaphore = VK_NULL_HANDLE, const VkFence& fence = VK_NULL_HANDLE);
		void Submit(const std::vector<VkCommandBuffer> commandBuffers);
		void Present(const SwapChain& swapChain, const uint32_t& imageIndex, const VkSemaphore& waitSemaphore);
		void WaitIdle();

		operator VkQueue();
		operator const VkQueue() const;
	};

}