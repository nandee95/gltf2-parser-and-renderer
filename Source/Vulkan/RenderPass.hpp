#pragma once
#include "Exception.hpp"
#include "Vulkan/Vulkan.hpp"

#include <glm/vec4.hpp>
#include <vector>

namespace Vulkan
{
	class Device;
	class FrameBuffer;
	class RenderPass
	{
	protected:
		// Parent
		Device* device = nullptr;
		// Vulkan objects
		VkRenderPass renderPass = VK_NULL_HANDLE;
	public:
		RenderPass();
		RenderPass(RenderPass& other) = delete;
		RenderPass(RenderPass&& other) noexcept;
		RenderPass(Device* device, const VkFormat& colorFormat, const VkFormat& depthFormat= VK_FORMAT_UNDEFINED);
		~RenderPass();

		void Create(Device* device, const VkFormat& colorFormat, const VkFormat& depthFormat = VK_FORMAT_UNDEFINED);
		void Destroy();
		bool IsValid() const;
		Device* GetDevice();

		void CmdBegin(const FrameBuffer& frameBuffer, const VkRect2D& renderArea, const std::vector<VkClearValue>& clearValues);
		void CmdEnd();

		RenderPass& operator =(RenderPass&& other) noexcept;
		operator VkRenderPass();
		operator const VkRenderPass() const;
	};
}