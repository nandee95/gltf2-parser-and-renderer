#pragma once

#include "Exception.hpp"
#include "Vulkan/Vulkan.hpp"
#include "Vulkan/ShaderModule.hpp"
#include "Vulkan/VertexInput.hpp"
#include "Vulkan/RenderPass.hpp"
#include "Vulkan/DescriptorSet.hpp"

namespace Vulkan
{
	struct ShaderStageInfo
	{
		const ShaderModule& shaderModule;
		VkShaderStageFlagBits stage;
		const char* entryPoint = "main";
	};

	class Pipeline
	{
	protected:
		Device* device = nullptr;
		VkPipeline pipeline = VK_NULL_HANDLE;
		VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
	public:

		void Create(Device* device, const VertexInput& vertexInput, const VkDescriptorSetLayout& descriptorSetLayout, const RenderPass& renderPass, const VkExtent2D& extent, std::initializer_list<ShaderStageInfo> stageInfos, const VkPrimitiveTopology& topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);

		void CmdBind(const VkPipelineBindPoint& bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS);

		~Pipeline();
		void Destroy();

		operator const VkPipelineLayout() const;

	};
}