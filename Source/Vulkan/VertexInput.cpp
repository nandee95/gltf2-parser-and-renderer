#include "Vulkan/VertexInput.hpp"

namespace Vulkan
{
	VertexInput::VertexInput() noexcept
	{
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	}
	void VertexInput::PushBinding(const uint32_t& binding, const uint32_t& stride, const VkVertexInputRate& inputRate) noexcept
	{
		VkVertexInputBindingDescription bindingDescription{};
		bindingDescription.binding = binding;
		bindingDescription.stride = stride;
		bindingDescription.inputRate = inputRate;
		bindings.push_back(bindingDescription);

		vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindings.size());
		vertexInputInfo.pVertexBindingDescriptions = bindings.data();
	}
	void VertexInput::PushAttrib(const uint32_t& binding, const uint32_t& location, const uint32_t& offset, const VkFormat& format) noexcept
	{
		VkVertexInputAttributeDescription attribDescription{};
		attribDescription.binding = binding;
		attribDescription.location = location;
		attribDescription.offset = offset;
		attribDescription.format = format;
		attribs.push_back(attribDescription);

		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribs.size());
		vertexInputInfo.pVertexAttributeDescriptions = attribs.data();
	}
	void VertexInput::SetInputRate(const VkVertexInputRate& inputRate) noexcept
	{
		bindingDescription.inputRate = inputRate;
	}
	VertexInput::operator const VkPipelineVertexInputStateCreateInfo* () const
	{
		return &vertexInputInfo;
	}
}