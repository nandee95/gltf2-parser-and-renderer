#include "Vulkan/Buffer.hpp"

#include "Vulkan/Device.hpp"
#include "Vulkan/Image.hpp"
#include "Vulkan/CommandBuffer.hpp"

namespace Vulkan
{
	Buffer::Buffer()
	{
	}

	Buffer::Buffer(Device* device, const VkDeviceSize& size, const VkBufferUsageFlags& usage)
	{
		Create(device, size, usage);
	}

	Buffer::~Buffer()
	{
		Destroy();
	}

	Buffer::Buffer(Buffer&& other) noexcept : buffer(other.buffer), memory(other.memory), device(other.device), size(other.size)
	{
		other.buffer = VK_NULL_HANDLE;
		other.memory = VK_NULL_HANDLE;
		other.size = 0;
		other.device = nullptr;
	}

	Buffer& Buffer::operator =(Buffer&& other) noexcept
	{
		buffer = other.buffer;
		memory = other.memory;
		size = other.size;
		device = other.device;

		other.buffer = VK_NULL_HANDLE;
		other.memory = VK_NULL_HANDLE;
		other.size = 0;
		other.device = nullptr;
		return *this;
	}

	void Buffer::Create(Device* device, const VkDeviceSize& size, const VkBufferUsageFlags& usage)
	{
		if (IsValid())
		{
			throw Exception( "Vulkan Buffer already exists!");
		}

		this->device = device;
		this->size = size;

		VkBufferCreateInfo bufferInfo{};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		VkResult result;
		if ((result = vkCreateBuffer(*device, &bufferInfo, nullptr, &buffer)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateBuffer returned ", result);
		}
	}

	void Buffer::Allocate(const VkMemoryPropertyFlags& flags)
	{
		VkMemoryRequirements memRequirements;
		vkGetBufferMemoryRequirements(*device, buffer, &memRequirements);

		VkMemoryAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = device->FindMemoryType(memRequirements.memoryTypeBits, flags);

		VkResult result;
		if ((result = vkAllocateMemory(*device,&allocInfo,nullptr,&memory)) != VK_SUCCESS)
		{
			throw Exception( "vkAllocateMemory returned ", result);
		}
		if ((result = vkBindBufferMemory(*device, buffer, memory, 0)) != VK_SUCCESS)
		{
			throw Exception( "vkBindBufferMemory returned ", result);
		}
	}

	void Buffer::Copy(Buffer& destination, const uint32_t& size)
	{
		CommandBuffer transferCmd(&device->GetTransferPool());
		transferCmd.Begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

		VkBufferCopy copyRegion{};
		copyRegion.size = size;
		vkCmdCopyBuffer(transferCmd, buffer, destination, 1, &copyRegion);

		transferCmd.End();

		Queue& transferQueue = device->GetTransferQueue();
		transferQueue.Submit(transferCmd);
		transferQueue.WaitIdle();
	}

	void Buffer::Destroy()
	{
		Free();
		if (buffer != VK_NULL_HANDLE)
		{
			vkDestroyBuffer(*device, buffer, nullptr);
			buffer = VK_NULL_HANDLE;
		}
		device = nullptr;
		size = 0;
	}

	void Buffer::Free()
	{
		if (memory != VK_NULL_HANDLE)
		{
			vkFreeMemory(*device, memory, nullptr);
			memory = VK_NULL_HANDLE;
		}
	}

	void Buffer::UploadWithMapping(const void* data, const VkDeviceSize& size,const VkDeviceSize& offset)
	{
		void* mappedMemory;
		VkResult result;
		if((result = vkMapMemory(*device, memory, offset, size, 0, &mappedMemory)) != VK_SUCCESS)
		{
			throw Exception("vkMapMemory returned",result);
		}
		std::memcpy(mappedMemory, data, static_cast<size_t>(size));
		vkUnmapMemory(*device, memory);
	}

	void Buffer::CmdCopyToImage(Vulkan::Image& image,const VkOffset3D& offset, const VkExtent3D& extent, const uint32_t& layerCount)
	{
		VkBufferImageCopy region{};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = layerCount;
		region.imageOffset = offset;
		region.imageExtent = extent;

		vkCmdCopyBufferToImage(*CommandBuffer::Active, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
	}

	bool Buffer::IsValid() const
	{
		return buffer != VK_NULL_HANDLE && device != nullptr;
	}

	const VkDeviceSize& Buffer::GetSize() const
	{
		return size;
	}

	Buffer::operator const VkBuffer&() const
	{
		return buffer;
	}
	Buffer::operator VkBuffer& ()
	{
		return buffer;
	}
	Buffer::operator const VkDeviceMemory&() const
	{
		return memory;
	}

}