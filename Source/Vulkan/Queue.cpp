#include "Vulkan/Queue.hpp"

namespace Vulkan
{
	Queue::operator VkQueue()
	{
		return queue;
	}
	Queue::operator const VkQueue() const
	{
		return queue;
	}
	uint32_t Queue::GetFamilyIndex() const
	{
		return familyIndex;
	}
	bool Queue::IsValid() const
	{
		return familyIndex != -1 && queue != VK_NULL_HANDLE;
	}

	Queue::operator bool()
	{
		return IsValid();
	}
	void Queue::Submit(const VkCommandBuffer& commandBuffer, const VkSemaphore& waitSemaphore, const VkSemaphore& signalSemaphore, const VkFence& fence)
	{
		VkSubmitInfo submitInfo{};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.pCommandBuffers = &commandBuffer;
		submitInfo.commandBufferCount = 1;

		submitInfo.pSignalSemaphores = &signalSemaphore;
		submitInfo.signalSemaphoreCount = signalSemaphore == VK_NULL_HANDLE ? 0 : 1;

		submitInfo.pWaitSemaphores = &waitSemaphore;
		submitInfo.waitSemaphoreCount = waitSemaphore == VK_NULL_HANDLE ? 0 : 1;

		VkPipelineStageFlags waitDstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		submitInfo.pWaitDstStageMask = &waitDstStageMask;
		vkQueueSubmit(queue, 1, &submitInfo, fence);
	}
	void Queue::Submit(const std::vector<VkCommandBuffer> commandBuffers)
	{

		VkSubmitInfo submitInfo{};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
		submitInfo.pCommandBuffers = commandBuffers.data();
		vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
	}
	void Queue::Present(const SwapChain& swapChain, const uint32_t& imageIndex, const VkSemaphore& waitSemaphore)
	{
		VkPresentInfoKHR presentInfo{};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.pImageIndices = &imageIndex;

		const VkSwapchainKHR swapChains = swapChain;
		presentInfo.pSwapchains = &swapChains;
		presentInfo.swapchainCount = 1;

		presentInfo.pWaitSemaphores = &waitSemaphore;
		presentInfo.waitSemaphoreCount = waitSemaphore == VK_NULL_HANDLE ? 0 : 1;

		vkQueuePresentKHR(queue, &presentInfo);
	}
	void Queue::WaitIdle()
	{
		vkQueueWaitIdle(queue);
	}
}