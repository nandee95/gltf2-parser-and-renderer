#include "Fence.hpp"

#include "Exception.hpp"

void Vulkan::Fence::Create(const Device& device, const VkFlags& flags)
{
	if (fence != VK_NULL_HANDLE)
	{
		throw Exception("Vulkan fence already exists!");
	}

	VkFenceCreateInfo fenceInfo{};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = flags;

	VkResult res;
	if ((res = vkCreateFence(device, &fenceInfo, nullptr, &fence)) != VK_SUCCESS)
	{
		throw Exception("Failed to create fence: ", res);
	}

	this->device = &device;
}

void Vulkan::Fence::Wait(const VkBool32& waitAll, const uint64_t& timeOut)
{
	VkResult res;
	if ((res = vkWaitForFences(*device, 1, &fence, waitAll, timeOut)) != VK_SUCCESS)
	{
		throw Exception("Failed to wait for fence:", res);
	}
}

void Vulkan::Fence::Reset()
{
	vkResetFences(*device, 1, &fence);
}

void Vulkan::Fence::Destroy()
{
	if (fence != VK_NULL_HANDLE)
	{
		vkDestroyFence(*device, fence, nullptr);
	}
}

Vulkan::Fence::~Fence()
{
	Destroy();
}


Vulkan::Fence::operator const VkFence() const
{
	return fence;
}