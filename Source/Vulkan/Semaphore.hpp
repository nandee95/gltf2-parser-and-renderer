#pragma once
#include "Vulkan/Vulkan.hpp"

namespace Vulkan
{
	class Device;
	class Semaphore
	{
	protected:
		VkSemaphore semaphore = VK_NULL_HANDLE;
		Device* device = nullptr;
	public:
		void Create(Device* device, const VkFlags& flags = 0);

		operator VkSemaphore()
		{
			return semaphore;
		}

		operator const VkSemaphore() const
		{
			return semaphore;
		}

		void Destroy();

		~Semaphore()
		{
			Destroy();
		}

	};
}