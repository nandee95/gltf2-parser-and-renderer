#include "Vulkan/Device.hpp"
#include "Vulkan/Surface.hpp"

namespace Vulkan
{
	Device::Device() noexcept
	{
	}

	Device::Device(Device&& other) noexcept :
		physicalDevice(other.physicalDevice), device(other.device), deviceProperties(other.deviceProperties), instance(other.instance),
		transferPool(std::move(other.transferPool)), graphicsQueue(std::move(other.graphicsQueue)), transferQueue(std::move(other.transferQueue)), presentQueue(std::move(other.presentQueue)), computeQueue(std::move(other.computeQueue))
	{
		other.device = VK_NULL_HANDLE;
		other.physicalDevice = PhysicalDevice();
		other.deviceProperties = VkPhysicalDeviceProperties{};
		other.instance = nullptr;
		other.transferPool = CommandPool();
	}

	Device::~Device() noexcept
	{
		Destroy();
	}

	void Device::Create(Instance* instance, const PhysicalDevice& physicalDevice, const uint32_t& queueFlags, const std::vector<const char*> deviceExtensions, const VkPhysicalDeviceFeatures& features, const Surface* surface)
	{
		if (IsValid())
		{
			throw Exception( "Vulkan Device already exists");
		}

		this->instance = instance;
		this->physicalDevice = physicalDevice;

		const auto queueFamilies = physicalDevice.GetQueueFamilyProperties();
		{
			int32_t id = 0;
			for (const auto& qf : queueFamilies)
			{
				if (qf.queueFlags & VK_QUEUE_GRAPHICS_BIT)
				{
					if (!graphicsQueue && (queueFlags & DeviceQueue::Graphics))		graphicsQueue.familyIndex = id;
				}
				else if (!transferQueue && (queueFlags & DeviceQueue::Transfer) && (qf.queueFlags & VK_QUEUE_TRANSFER_BIT))
				{
					transferQueue.familyIndex = id;
				}

				if (!computeQueue && (queueFlags & DeviceQueue::Compute) && (qf.queueFlags & VK_QUEUE_COMPUTE_BIT))
				{
					computeQueue.familyIndex = id;
				}

				if (!presentQueue && (queueFlags & DeviceQueue::Present))
				{
					VkBool32 presentSupport = false;
					vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, id, *surface, &presentSupport);
					if (presentSupport)
						presentQueue.familyIndex = id;
				}

				id++;
			}
		}

		if (transferQueue.familyIndex == -1 && graphicsQueue.familyIndex != -1 && (queueFamilies[graphicsQueue.familyIndex].queueFlags & VK_QUEUE_TRANSFER_BIT))
			transferQueue.familyIndex = graphicsQueue.familyIndex;

		// Find unique queue families
		std::vector<VkDeviceQueueCreateInfo> uniqueQueueInfos;
		float queuePriority = 1.0f;
		{
			VkDeviceQueueCreateInfo queueInfo{};
			queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfo.queueCount = 1;
			queueInfo.pQueuePriorities = &queuePriority;
			for (const auto& q : { graphicsQueue ,transferQueue, presentQueue, computeQueue })
			{
				if (q.familyIndex == -1) continue;

				queueInfo.queueFamilyIndex = q.familyIndex;
				if (std::find_if(uniqueQueueInfos.begin(), uniqueQueueInfos.end(), [queueInfo](const VkDeviceQueueCreateInfo& i) { return i.queueFamilyIndex == queueInfo.queueFamilyIndex; }) == uniqueQueueInfos.end())
					uniqueQueueInfos.push_back(queueInfo);
			}
		}

		//Create device
		VkDeviceCreateInfo deviceInfo{};
		deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceInfo.pQueueCreateInfos = uniqueQueueInfos.data();
		deviceInfo.queueCreateInfoCount = static_cast<uint32_t>(uniqueQueueInfos.size());
		deviceInfo.pEnabledFeatures = &features;
		deviceInfo.ppEnabledExtensionNames = deviceExtensions.data();
		deviceInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());

		VkResult result;
		if ((result = vkCreateDevice(physicalDevice, &deviceInfo, nullptr, &device)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateDevice returned ", result);
		}

		// Get device properties
		vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

		// Get device queues
		for (Queue* q : { &graphicsQueue ,&transferQueue, &presentQueue, &computeQueue })
		{
			if (q->familyIndex == -1) continue;
			vkGetDeviceQueue(device, q->familyIndex, 0, &q->queue);
		}

		// Create transfer pool
		if (transferQueue)
			transferPool.Create(this, transferQueue);
	}

	void Device::Destroy() noexcept
	{
		if (transferPool)
		{
			transferPool.Destroy();
		}

		if (device != VK_NULL_HANDLE)
		{
			vkDestroyDevice(device, nullptr);
			device = VK_NULL_HANDLE;
		}

		instance = nullptr;
	}

	bool Device::IsValid() const noexcept
	{
		return device != VK_NULL_HANDLE;
	}

	Device::operator bool() const noexcept
	{
		return IsValid();
	}

	Instance* Device::GetInstance() noexcept
	{
		return instance;
	}

	Queue& Device::GetGraphicsQueue() noexcept
	{
		return graphicsQueue;
	}

	Queue& Device::GetTransferQueue() noexcept
	{
		return transferQueue;
	}

	Queue& Device::GetPresentQueue() noexcept
	{
		return presentQueue;
	}

	Queue& Device::GetComputeQueue() noexcept
	{
		return computeQueue;
	}

	CommandPool& Device::GetTransferPool() noexcept
	{
		return transferPool;
	}

	PhysicalDevice& Device::GetPhysicalDevice() noexcept
	{
		return physicalDevice;
	}

	VkPhysicalDeviceProperties Device::GetProperties() noexcept
	{
		return deviceProperties;
	}

	int32_t Device::FindMemoryType(const uint32_t& typeFilter, const VkMemoryPropertyFlags& properties) const
	{
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
			if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
				return i;
			}
		}

		throw Exception( "Failed to find memory type");
	}

	VkFormat Device::FindSupportedFormat(const std::vector<VkFormat>& candidates, const VkImageTiling& tiling, const VkFormatFeatureFlags& features)
	{
		VkFormatProperties properties;
		for (const VkFormat& format : candidates)
		{
			vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &properties);

			switch (tiling)
			{
			case VK_IMAGE_TILING_LINEAR:
			{
				if ((properties.linearTilingFeatures & features) == features) return format;
				break;
			}
			case VK_IMAGE_TILING_OPTIMAL:
			{
				if ((properties.optimalTilingFeatures & features) == features) return format;
				break;
			}
			}

		}
		throw Exception( "Failed to find supported format!");
	}

	void Device::WaitIdle()
	{
		VkResult result;
		if ((result = vkDeviceWaitIdle(device)) != VK_SUCCESS)
		{
			throw Exception( "vkDeviceWaitIdle returned ", result);
		}
	}

	Device::operator const VkDevice& () const noexcept
	{
		return device;
	}

	Device::operator VkDevice& () noexcept
	{
		return device;
	}

	Device& Device::operator =(Device&& other) noexcept
	{
		Destroy();
		graphicsQueue = std::move(other.graphicsQueue);
		transferQueue = std::move(other.transferQueue);
		computeQueue = std::move(other.computeQueue);
		presentQueue = std::move(other.presentQueue);

		device = other.device;
		physicalDevice = other.physicalDevice;
		deviceProperties = other.deviceProperties;
		instance = other.instance;
		transferPool = std::move(other.transferPool);
		other.device = VK_NULL_HANDLE;
		other.deviceProperties = VkPhysicalDeviceProperties{};
		other.instance = nullptr;

		return *this;
	}
}