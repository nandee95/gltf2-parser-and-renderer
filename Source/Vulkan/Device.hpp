/*****************************************************************//**
* @file   Device.hpp
* @brief  Vulkan Device
* 
* @author Nandee
* @date   July 2020
*********************************************************************/
#pragma once
#include "Vulkan/Vulkan.hpp"
#include "Vulkan/Instance.hpp"
#include "Vulkan/Queue.hpp"
#include "Vulkan/PhysicalDevice.hpp"
#include "Vulkan/CommandPool.hpp"

#include <glm/vec2.hpp>
#include <vector>

namespace Vulkan
{
	enum DeviceQueue : uint32_t
	{
		Graphics = 1 << 0,
		Present = 1 << 1,
		Transfer = 1 << 2,
		Compute = 1 << 3
	};

	class Surface;
	class Device
	{
	public:
	protected:
		/**
		 * @brief Pointer to the parent Instance.
		 */
		Instance* instance;

		/**
		 * @brief Vulkan device object
		 */
		VkDevice device = VK_NULL_HANDLE;

		/**
		 * @brief Vulkan Device properties
		 */
		VkPhysicalDeviceProperties deviceProperties{};

		/**
		 * @brief Compute queue
		 */
		PhysicalDevice physicalDevice;

		/**
		 * @brief Transfer pool
		 */
		CommandPool transferPool;

		/**
		 * @brief Graphics queue
		 */
		Queue graphicsQueue;

		/**
		 * @brief Transfer queue
		 */
		Queue transferQueue;

		/**
		 * @brief Present queue
		 */
		Queue presentQueue;

		/**
		 * @brief Compute queue
		 */
		Queue computeQueue;
	public:
		/**
		* Empty constructor.
		*/
		Device() noexcept;

		/**
		* Copy constructor. Copying a device is not allowed.
		*
		* @param other	Device to copy
		*/
		Device(Device& other) = delete;

		/**
		* Move constructor. Moves the other device into the current device.
		*
		* @param other	Device to move
		*/
		Device(Device&& other) noexcept;

		/**
		* @copydoc Vulkan::Device::Create()
		*/
		Device(Instance* instance, const PhysicalDevice& physicalDevice, const uint32_t& queueFlags, const std::vector<const char*> deviceExtensions, const VkPhysicalDeviceFeatures& features, const Surface* surface = nullptr) : physicalDevice(physicalDevice), instance(instance)
		{
			Create(instance, physicalDevice, queueFlags, deviceExtensions, features, surface);
		}

		/**
		* Destroys the device.
		*/
		~Device() noexcept;

		/**
		* Creates a device. The queues are populated according to the queueFlags parameter.
		*
		* @throw VulkanDeviceAlreadyExists		The device was created before.
		* @throw VulkanFailedToCreateDevice		Vulkan command failed
		* @param instance						parent Instance
		* @param physicalDevice					selected PhysicalDevice
		* @param queueFlags						Queues to create
		* @param deviceExtensions				requested device extensions
		* @param features						device features to enable
		* @param surface						Surface (Optional)
		*/
		void Create(Instance* instance, const PhysicalDevice& physicalDevice, const uint32_t& queueFlags, const std::vector<const char*> deviceExtensions, const VkPhysicalDeviceFeatures& features, const Surface* surface = nullptr);

		/**
		* Destroys the device.
		*/
		void Destroy() noexcept;

		/**
		* Validates the device.
		*
		* @return	true if the device is valid
		*/
		bool IsValid() const noexcept;

		/**
		* Validates the device.
		*
		* @return	true if the device is valid
		*/
		operator bool() const noexcept;

		/**
		* Returns the parent instance of the device.
		*
		* @return	Parent instance
		*/
		Instance* GetInstance() noexcept;

		/**
		* Returns the graphics queue. Only valid if DeviceQueue::Graphics flag is set in queueFlags.
		*
		* @return	Graphics queue
		*/
		Queue& GetGraphicsQueue() noexcept;

		/**
		* Returns the created transfer queue. Only valid if DeviceQueue::Transfer flag is set in queueFlags.
		* If there is a queue which doesn't support graphics operations but supports transfer It will be selected. Same as the graphics queue otherwise.
		*
		* @return	Transfer queue
		*/
		Queue& GetTransferQueue() noexcept;

		/**
		* Returns the created present queue. Only valid if DeviceQueue::Present flag is set in queueFlags.
		*
		* @return	Present queue
		*/
		Queue& GetPresentQueue() noexcept;

		/**
		* Returns the the created present queue. Only valid if DeviceQueue::Compute flag is set in queueFlags.
		*
		* @return	Present queue
		*/
		Queue& GetComputeQueue() noexcept;

		/**
		* Returns the pool reserved for transfer operations.
		*
		* @return	Transfer pool
		*/
		CommandPool& GetTransferPool() noexcept;

		/**
		* Returns the selected physical device.
		*
		* @return					selected PhysicalDevice
		*/
		PhysicalDevice& GetPhysicalDevice() noexcept;

		/**
		* Returns the device properties. (Stored in memory)
		*
		* @return					PhysicalDevice properties
		*/
		VkPhysicalDeviceProperties GetProperties() noexcept;

		/**
		* Finds a memory index with a given type and properties.
		*
		* @param typeFilter			memory type
		* @param properties			property flags
		* @return					Found memory index
		*/
		int32_t FindMemoryType(const uint32_t& typeFilter, const VkMemoryPropertyFlags& properties) const;

		/**
		* Finds a supported VkFormat.
		*
		* @throw VulkanFailedToFindFormat 			failed to find format
		*
		* @param	candidates							list of the candidates
		* @param	tiling								image tiling
		* @param	features							format feature flags
		* @return 										found format
		*/
		VkFormat FindSupportedFormat(const std::vector<VkFormat>& candidates, const VkImageTiling& tiling, const VkFormatFeatureFlags& features);

		/**
		* Blocks the current thread until the device is back to idle state.
		*
		* @throw	VulkanDeviceFailedToWaitIdle		Vulkan command failed
		*/
		void WaitIdle();

		/**
		* Returns the underlying VkDevice.
		*
		* @return	VkDevice object
		*/
		operator const VkDevice& () const noexcept;

		/**
		* Returns the underlying VkDevice.
		*
		* @return	VkDevice object
		*/
		operator VkDevice& () noexcept;

		/**
		* Move equal operator. Destroys the current device and moves the other device into itself.
		*
		* @param	other	Device to move
		* @return	Reference to the moved device
		*/
		Device& operator =(Device&& other) noexcept;
	};
}