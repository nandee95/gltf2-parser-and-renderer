#include "Vulkan/SwapChain.hpp"
#include "Vulkan/Device.hpp"

namespace Vulkan
{
	void SwapChain::Create(Device* device, const Surface& surface, const uint32_t& imageCount, const VkFormat& format, const VkColorSpaceKHR& colorSpace, const VkExtent2D& extent, const VkSurfaceTransformFlagBitsKHR& preTransform, const VkPresentModeKHR& presentMode)
	{
		this->device = device;
		VkSwapchainCreateInfoKHR swapchainInfo{};
		swapchainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapchainInfo.surface = surface;

		swapchainInfo.minImageCount = imageCount;
		swapchainInfo.imageFormat = format;
		swapchainInfo.imageColorSpace = colorSpace;
		swapchainInfo.imageExtent = extent;
		swapchainInfo.imageArrayLayers = 1;
		swapchainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		swapchainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

		swapchainInfo.preTransform = preTransform;
		swapchainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		swapchainInfo.presentMode = presentMode;
		swapchainInfo.clipped = VK_TRUE;

		swapchainInfo.oldSwapchain = VK_NULL_HANDLE;

		VkResult result;
		if ((result = vkCreateSwapchainKHR(*device, &swapchainInfo, nullptr, &swapchain)) != VK_SUCCESS)
		{
			throw Exception( "vkCreateSwapchainKHR returned ",result);
		}

		uint32_t count = 0;
		if ((result = vkGetSwapchainImagesKHR(*device, swapchain, &count, nullptr)) != VK_SUCCESS)
		{
			throw Exception( "vkGetSwapchainImagesKHR returned ", result);
		}
		images.resize(imageCount);

		if ((result = vkGetSwapchainImagesKHR(*device, swapchain, &count, images.data())) != VK_SUCCESS)
		{
			throw Exception( "vkGetSwapchainImagesKHR returned ", result);
		}
		imageViews.resize(images.size());
		count = 0;
		for (auto& i : images)
		{
			VkImageViewCreateInfo imageViewInfo{};
			imageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imageViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewInfo.subresourceRange.baseMipLevel = 0;
			imageViewInfo.subresourceRange.levelCount = 1;
			imageViewInfo.subresourceRange.baseArrayLayer = 0;
			imageViewInfo.subresourceRange.layerCount = 1;
			imageViewInfo.format = format;
			imageViewInfo.image = i;
			if ((result = vkCreateImageView(*device, &imageViewInfo, nullptr, &imageViews[count])) != VK_SUCCESS)
			{
				throw Exception( "vkCreateImageView returned ", result);
			}
			count++;
		}
	}
	uint32_t SwapChain::AcquireNextImage(const VkSemaphore& semaphore, const VkFence& fence, const uint64_t& timeOut)
	{
		uint32_t imageIndex;
		VkResult result;
		if ((result = vkAcquireNextImageKHR(*device, swapchain, UINT64_MAX, semaphore, fence, &imageIndex)) != VK_SUCCESS)
		{
			throw Exception( "vkAcquireNextImageKHR returned ", result);
		}
		return imageIndex;
	}
	void SwapChain::Destroy()
	{
		if (swapchain != VK_NULL_HANDLE)
		{
			vkDestroySwapchainKHR(*device, swapchain, nullptr);
			swapchain = VK_NULL_HANDLE;
		}
		for (auto& iv : imageViews)
		{
			vkDestroyImageView(*device, iv, nullptr);
		}
		imageViews.clear();
		images.clear();
		device = nullptr;
	}
}