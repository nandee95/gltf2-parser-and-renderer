#pragma once
#include "Exception.hpp"
#include "Vulkan/Vulkan.hpp"

#include <vector>
#include <glm/vec4.hpp>

namespace Vulkan
{
	class CommandPool;
	class CommandBuffer
	{
	protected:
		// Parent
		CommandPool* commandPool = nullptr;

		// Vulkan object
		VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
	public:
		// Currently active buffer (Activated with Begin())
		static thread_local CommandBuffer* Active;

		/**
		* Empty constructor.
		*/
		CommandBuffer() noexcept;

		/**
		* Copy constructor. Copying a command buffer is not allowed.
		*
		* @param other	Command buffer to copy
		*/
		CommandBuffer(const CommandBuffer&) = delete;

		/**
		* Move constructor. Moves the other command buffer into the current command buffer.
		*
		* @param other	Command buffer to move
		*/
		CommandBuffer(CommandBuffer&& other) noexcept;

		/**
		* Allocate constructor. Allocates the command buffer.
		* 
		* @param commandPool	Command pool
		* @param level			Command buffer level
		*/
		CommandBuffer(CommandPool* commandPool, const VkCommandBufferLevel& level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
		
		/**
		* Frees the command buffer.
		*/
		~CommandBuffer() noexcept;

		/**
		* Allocates the command buffer.
		* Exceptions:
		*		VulkanCommandBufferAlreadyExists
		*		VulkanFailedToAllocateCommandBuffers
		*
		* @param commandPool	Command pool
		* @param level			Command buffer level
		*/
		void Allocate(CommandPool* commandPool, const VkCommandBufferLevel& level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);

		/**
		* Frees the command buffer. 
		*/
		void Free() noexcept;

		/**
		* Begins the recording of the command buffer.
		* Exceptions:
		*		VulkanFailedToBeginCommandBuffer
		*
		* @param flags Buffer usage flags
		*/	
		void Begin(const VkCommandBufferUsageFlags& flags = 0);

		/**
		* Ends the recording of the command buffer.
		* Exceptions:
		*		VulkanFailedToEndCommandBuffer
		*/
		void End();

		/**
		* Validates the command buffer.
		*
		* @return	true if the command buffer is valid
		*/
		bool IsValid() const noexcept;

		/**
		* Validates the command buffer.
		*
		* @return	true if the command buffer is valid
		*/
		operator bool() const noexcept;

		/**
		* Returns the parent command pool of the command buffer.
		*
		* @return	Parent command pool
		*/
		CommandPool* GetCommandPool() noexcept;

		/**
		* Returns the underlying VkCommandBuffer.
		*
		* @return	VkCommandBuffer object
		*/
		operator const VkCommandBuffer&() const noexcept;

		/**
		* Returns the underlying VkCommandBuffer.
		*
		* @return	VkCommandBuffer object
		*/
		operator VkCommandBuffer&() noexcept;

		/**
		* Move equal operator. Destroys the current command buffer and moves the other command buffer into itself.
		*
		* @param	other	Command buffer to move
		* @return			Reference to the moved command buffer
		*/
		CommandBuffer& operator=(CommandBuffer&& other) noexcept;
	};
}