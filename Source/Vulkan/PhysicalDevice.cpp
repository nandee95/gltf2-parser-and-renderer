#include "PhysicalDevice.hpp"
#include "Device.hpp"

#include <algorithm>
#include <cstring>

namespace Vulkan
{

	VkPhysicalDeviceProperties PhysicalDevice::GetProperties() const noexcept
	{
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties(physicalDevice, &props);
		return props;
	}
	VkPhysicalDeviceProperties2 PhysicalDevice::GetProperties2() const noexcept
	{
		VkPhysicalDeviceProperties2 props;
		vkGetPhysicalDeviceProperties2(physicalDevice, &props);
		return props;
	}
	VkSurfaceCapabilitiesKHR PhysicalDevice::GetSurfaceCapabilities(const VkSurfaceKHR& surface) const noexcept
	{
		VkSurfaceCapabilitiesKHR caps;
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &caps);
		return caps;
	}
	std::vector<VkSurfaceFormatKHR> PhysicalDevice::GetSurfaceFormats(const VkSurfaceKHR& surface) const
	{
		uint32_t count;
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &count, nullptr);
		if (count == 0)
			return {};
		std::vector<VkSurfaceFormatKHR> surfaceFormats(count);
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &count, surfaceFormats.data());
		return surfaceFormats;
	}

	std::vector<VkPresentModeKHR> PhysicalDevice::GetSurfacePresentModes(const VkSurfaceKHR& surface) const
	{
		uint32_t count;
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &count, nullptr);
		if (count == 0)
			return {};
		std::vector<VkPresentModeKHR> presentModes(count);
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &count, presentModes.data());
		return presentModes;
	}

	std::vector<VkQueueFamilyProperties> PhysicalDevice::GetQueueFamilyProperties() const
	{
		uint32_t count;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, nullptr);
		if (count == 0)
			return {};
		std::vector<VkQueueFamilyProperties> queueFamilies(count);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, queueFamilies.data());
		return queueFamilies;
	}

	std::vector<VkExtensionProperties> PhysicalDevice::GetExtensions() const
	{
		uint32_t count;
		vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &count, nullptr);
		if (count == 0)
			return {};
		std::vector<VkExtensionProperties> extensionProperties(count);
		vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &count, extensionProperties.data());
		return extensionProperties;
	}

	VkSampleCountFlagBits PhysicalDevice::GetMaxSampleCount() const
	{
		auto physicalDeviceProperties = GetProperties();

		VkSampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts &
			physicalDeviceProperties.limits.framebufferDepthSampleCounts;
		if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
		if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
		if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
		if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
		if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
		if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }

		return VK_SAMPLE_COUNT_1_BIT;
	}

	PhysicalDevice::PhysicalDevice(Instance* instance, const VkPhysicalDevice& physicalDevice) : instance(instance), physicalDevice(physicalDevice) {}

	PhysicalDevice::PhysicalDevice() {}

	Instance* PhysicalDevice::GetInstance()
	{
		return instance;
	}

	PhysicalDevice::operator const VkPhysicalDevice() const
	{
		return physicalDevice;
	}
	PhysicalDevice::operator VkPhysicalDevice()
	{
		return physicalDevice;
	}
}