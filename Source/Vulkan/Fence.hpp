#pragma once

#include "Device.hpp"

namespace Vulkan
{

	class Fence
	{
	protected:
		VkFence fence = VK_NULL_HANDLE;
		const Device* device=nullptr;
	public:
		void Create(const Device& device, const VkFlags& flags = 0);

		void Wait(const VkBool32& waitAll, const uint64_t& timeOut = UINT64_MAX);

		void Reset();

		operator const VkFence() const;

		void Destroy();

		~Fence();

	};
}