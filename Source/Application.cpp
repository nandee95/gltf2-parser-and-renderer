#include "Application.hpp"
#include <iostream>
#include <chrono>
#include <algorithm>

#include "Camera.hpp"

#include <glm/mat4x4.hpp>

#include "Vulkan/Instance.hpp"
#include "Vulkan/PhysicalDevice.hpp"
#include "Vulkan/Device.hpp"
#include "Vulkan/RenderPass.hpp"
#include "Vulkan/FrameBuffer.hpp"
#include "Vulkan/CommandPool.hpp"
#include "Vulkan/CommandBuffer.hpp"
#include "Vulkan/DepthBuffer.hpp"
#include "Vulkan/Semaphore.hpp"


#include "Grid.hpp"
#include "Sky.hpp"
#include "Vulkan/CubeMap.hpp"

#include "Gltf2/Gltf2Model.hpp"



static VkBool32 VulkanValidationCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT           messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT                  messageTypes,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData);

Application::Application()
{
}

int32_t Application::Run()
{
	try
	{
		return RunInternal();
	}
	catch(Exception& e)
	{
		std::cerr << "Exception thrown: " << e.what() << std::endl;
	}
	return EXIT_FAILURE;
}

int32_t Application::RunInternal()
{
	Vulkan::CubeMap::Init();
	Vulkan::Texture::Init();
	Window::Init();

	// Create window
	window.Create({ 1280,720 }, "Gltf2 Parser And Renderer");
	
	//Create Vulkan instance
	Vulkan::Instance instance;

	{
		std::vector<const char*> layers;
		layers.push_back(Vulkan::InstanceLayer::Validation);
		layers.push_back(Vulkan::InstanceLayer::Monitor);
		
		std::vector<const char*> extensions = Window::GetInstanceExtensions();
		extensions.push_back(Vulkan::InstanceExtension::DebugUtils);
		
		instance.Create(
			"GAMF",
			VK_MAKE_VERSION(1, 0, 0),
			"Gltf2 Parser and Renderer",
			VK_MAKE_VERSION(1, 0, 0),
			VK_API_VERSION_1_0,
			layers,
			extensions
		);

		std::cout << "Instance layers: \n";
		for (const auto& e : layers)
		{
			std::cout << '\t' << e << '\n';
		}
		std::cout << "Instance extensions: \n";
		for (const auto& e : extensions)
		{
			std::cout << '\t' << e << '\n';
		}

		instance.CreateDebugMessenger(
			VulkanValidationCallback,
			this,
			Vulkan::MsgSeverity::AllErrors,
			Vulkan::MsgType::All
		);
	}


	// Find Vulkan physical device
	Vulkan::PhysicalDevice physicalDevice;
	{
		const std::vector<Vulkan::PhysicalDevice> physicalDevices = instance.GetPhysicalDevices();

		if (physicalDevices.empty())
		{
			throw Exception("No Vulkan capable device found!");
		}
		physicalDevice = physicalDevices[0];
	}
	
	std::cout << "Physical device selected:\n\t" << physicalDevice.GetProperties().deviceName << '\n';

	// Create surface
	Vulkan::Surface surface;
	surface.Create(&instance, window.GetHandle());

	// Create logical device
	Vulkan::Device device;
	{
		std::vector<const char*> extensions;
		extensions.push_back(Vulkan::DeviceExtension::Swapchain);
		VkPhysicalDeviceFeatures features{};
		features.samplerAnisotropy = VK_TRUE;
		
		device.Create(&instance,
			physicalDevice,
			Vulkan::DeviceQueue::Present | Vulkan::DeviceQueue::Graphics | Vulkan::DeviceQueue::Transfer,
			extensions,
			features,
			&surface
		);
	}

	// Create swap chain
	Vulkan::SwapChain swapChain;
	VkExtent2D extent{ 1280,720 };
	uint32_t imgCount;
	VkFormat format;
	{
		auto caps = physicalDevice.GetSurfaceCapabilities(surface);

		// Choose images count
		imgCount = caps.minImageCount + 1;
		if (imgCount > caps.maxImageCount && caps.maxImageCount != 0) imgCount = caps.maxImageCount;

		// Choose format
		auto formats = physicalDevice.GetSurfaceFormats(surface);
		auto surfaceformat = std::find_if(formats.begin(), formats.end(), [](const VkSurfaceFormatKHR& f) { return f.format == VK_FORMAT_B8G8R8A8_SRGB && f.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR; });
		if (surfaceformat == formats.end())
		{
			throw Exception("Can't find surface format!");
		}
		format = surfaceformat->format;

		// Choose present mode
		auto presentModes = physicalDevice.GetSurfacePresentModes(surface);
		VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
		for (const auto& p : presentModes) {
			if (p == VK_PRESENT_MODE_MAILBOX_KHR)
			{
				presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}
		}

		// Choose extents
		if (caps.currentExtent.width != UINT32_MAX) {
			extent = caps.currentExtent;
		}
		else
		{
			extent.width = std::clamp(extent.width, caps.minImageExtent.width, caps.maxImageExtent.width);
			extent.height = std::clamp(extent.height, caps.minImageExtent.height, caps.maxImageExtent.height);
		}

		swapChain.Create(&device, surface, imgCount, surfaceformat->format, surfaceformat->colorSpace, extent, caps.currentTransform, presentMode);
	}

	// Create command pool
	Vulkan::CommandPool commandPool;
	commandPool.Create(&device, device.GetGraphicsQueue());
	
	// Create depth buffer
	Vulkan::DepthBuffer depthBuffer;
	depthBuffer.Create(&device, commandPool, { 1280,720 });
	
	// Create Render pass
	Vulkan::RenderPass renderPass;
	renderPass.Create(&device, format,depthBuffer.GetFormat());

	ViewDescriptor viewDescriptorData;

	RenderInfo info;
	info.renderPass = &renderPass;
	info.graphicsPool = &commandPool;
	info.imgCount = imgCount;
	
	std::vector<Vulkan::Buffer> viewUniform(imgCount);

	for(auto& vu : viewUniform)
	{
		vu.Create(&device, sizeof(ViewDescriptor), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);
		vu.Allocate(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		info.viewUniformBuffers.push_back(vu);
	}
	
	Grid grid;
	grid.Create(&device,info, 10, 100);

	Sky sky;
	sky.Create(&device,info);

	Gltf2::Model model;
	model.Load(&device, info, "../../Resources/Models/GamfLogo.gltf", sky);

	Gltf2::Model model2;
	model2.Load(&device, info, "../../Resources/Models/DamagedHelmet.gltf", sky);
	

	std::vector< Vulkan::CommandBuffer> commandBuffers(imgCount);
	std::vector< Vulkan::CommandBuffer> commandBuffers2(imgCount);
	std::vector< Vulkan::Semaphore> renderSemaphores(imgCount);
	std::vector< Vulkan::Semaphore> ackImgSeamaphores(imgCount);
	std::vector< Vulkan::FrameBuffer> frameBuffers(imgCount);

	for (uint32_t i = 0; i < imgCount; i++)
	{
		frameBuffers[i].Create(&renderPass, { 1280,720 }, { swapChain[i].imageView, depthBuffer });
		{
			commandBuffers[i].Allocate(&commandPool);
			commandBuffers[i].Begin();

			VkClearValue depthStencil;
			depthStencil.depthStencil = { 1.f,0 };
			renderPass.CmdBegin(frameBuffers[i], { {0,0},{1280,720} }, { {0,0,0,1} ,depthStencil });

			info.currentImage = i;

			sky.CmdDraw(info);
			grid.CmdDraw(info);
			model.CmdDraw(info);

			renderPass.CmdEnd();

			commandBuffers[i].End();
		}
		{
			commandBuffers2[i].Allocate(&commandPool);
			commandBuffers2[i].Begin();

			VkClearValue depthStencil;
			depthStencil.depthStencil = { 1.f,0 };
			renderPass.CmdBegin(frameBuffers[i], { {0,0},{1280,720} }, { {0,0,0,1} ,depthStencil });

			info.currentImage = i;

			sky.CmdDraw(info);
			grid.CmdDraw(info);
			model2.CmdDraw(info);

			renderPass.CmdEnd();

			commandBuffers2[i].End();
		}

		renderSemaphores[i].Create(&device);
		ackImgSeamaphores[i].Create(&device);

	}

	auto last = std::chrono::high_resolution_clock::now();
	

	
	uint32_t lastImg = imgCount - 1;

	
	float dT = 0.f;

	std::vector<Vulkan::CommandBuffer>* currentCmdBuffer = &commandBuffers;
	viewDescriptorData.scale = 5.f;
	
	Camera camera;
	const glm::mat4 projection = camera.CalcProjMatrix(1280.f / 720.f);
	glm::vec2 lastCursorPos{ };
	glm::vec2 cameraAngle{ 0,0 };
	glm::vec2 cameraAngleTarget{ glm::half_pi<float>(),glm::pi<float>()*0.75f };
	window.SetCursorPosCallback([&](glm::vec2 pos)
	{
		if(window.IsLeftMouseButtonPressed())
		{
			const glm::vec2 newPos{
				pos.x < 0 ? pos.x + 1280.f : (pos.x >= 1280.f ? pos.x - 1280.f : pos.x),
				pos.y < 0 ? pos.y + 720.f : (pos.y >= 720.f ? pos.y - 720.f : pos.y)
			};

			glm::vec2 dPos;
			if (newPos != pos)
			{
				window.SetCursorPos(newPos);

				const glm::vec2 oldPos{
					pos.x < 0 ? lastCursorPos.x + 1280.f : (pos.x >= 1280.f ? lastCursorPos.x - 1280.f : lastCursorPos.x),
					pos.y < 0 ? lastCursorPos.y + 720.f : (pos.y >= 720.f ? lastCursorPos.y - 720.f : lastCursorPos.y)
				};

				dPos = newPos - oldPos;

				lastCursorPos = newPos;
			}
			else
			{
				dPos = pos - lastCursorPos;
				lastCursorPos = pos;
			}

			cameraAngleTarget.x += dPos.x * glm::pi<float>() / 512.f;
			cameraAngleTarget.y -= dPos.y * glm::pi<float>() / 512.f;
		}
		else lastCursorPos = pos;
	});
	window.SetKeyCallback([&](int key, int scancode, int action, int mods) {
		if (key == GLFW_KEY_M && action == GLFW_PRESS)
		{
			if (currentCmdBuffer == &commandBuffers)
			{
				currentCmdBuffer = &commandBuffers2;
				viewDescriptorData.scale = 500.f;
			}
			else
			{
				currentCmdBuffer = &commandBuffers;
				viewDescriptorData.scale = 5.f;
			}
		}
		if (key == GLFW_KEY_S && action == GLFW_PRESS)
		{
			viewDescriptorData.shading = (viewDescriptorData.shading + 1) % 3;
		}
		});

	camera.SetPosition({ 1000,0,0 });

	while (window.IsOpen())
	{
		// Calculate delta time
		auto now = std::chrono::high_resolution_clock::now();
		dT = std::chrono::duration_cast<std::chrono::microseconds>(now - last).count() / 1e6;
		last = now;

		// Acquire next framebuffer
		uint32_t imageIndex = swapChain.AcquireNextImage(ackImgSeamaphores[lastImg]);

		// Update camera
		const glm::vec2 cameraAngleDiff = cameraAngleTarget - cameraAngle;

		glm::vec2 factor{dT};

		factor *= glm::pow(glm::abs(cameraAngleDiff*16.f),{2,2});
		factor *= 0.5; // speed		
		factor = glm::clamp(factor, { 0,0 }, { 1,1 });

		cameraAngle += cameraAngleDiff * factor;
		
		glm::quat orient(0,1,0,0);

		orient = glm::rotate(orient, cameraAngle.y, glm::vec3(0, 0, 1));
		orient = glm::rotate(orient, cameraAngle.x, glm::vec3(0, 1, 0));

		camera.SetOrientation(orient);
		camera.SetPosition(glm::vec3(-1000, 0, 0)* camera.GetOrientation());
				
		viewDescriptorData.projView = projection * camera.CalcViewMatrix();
		viewDescriptorData.camPos = camera.GetPosition();
		viewUniform[imageIndex].UploadWithMapping( &viewDescriptorData, sizeof(viewDescriptorData));

		// Submit render command
		device.GetGraphicsQueue().Submit((*currentCmdBuffer)[imageIndex], ackImgSeamaphores[lastImg],renderSemaphores[lastImg]);
		
		// Submit present command
		device.GetPresentQueue().Present(swapChain, imageIndex, renderSemaphores[lastImg]);
		lastImg = imageIndex;

		// Wait for GPU to finish
		device.WaitIdle();

		// Handle window events
		window.PollEvents();
	}

	return EXIT_SUCCESS;
}


static VkBool32 VulkanValidationCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT           messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT                  messageTypes,
	const VkDebugUtilsMessengerCallbackDataEXT*      pCallbackData,
	void*                                            pUserData)
{
	Application* that = reinterpret_cast<Application*>(pUserData);

	std::cerr << "Vulkan Validation:\n\t" << pCallbackData->pMessage << '\n';

	return VK_TRUE;
}