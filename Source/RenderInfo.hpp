#pragma once

#include "Vulkan/RenderPass.hpp"
#include "Vulkan/DescriptorSet.hpp"


#include <glm/glm.hpp>

struct ViewDescriptor
{
	glm::mat4 projView;
	glm::vec3 camPos;
	int32_t shading=0;
	float scale=1.0;
};

struct RenderInfo
{
	Vulkan::RenderPass* renderPass;
	Vulkan::CommandPool* graphicsPool;
	uint32_t imgCount;
	uint32_t currentImage;
	std::vector<VkBuffer> viewUniformBuffers;
};
