set(VULKAN_TARGET "Vulkan")
if(TARGET ${VULKAN_TARGET})
	return()
endif()

# Message
color_message("Loading Vulkan... ")

# Options
set(VK_SDK_PATH "$ENV{VK_SDK_PATH}" CACHE STRING "Vulkan SDK location (eg. C:/SDK/Vulkan/x.x.x/)")

# Check SDK directory
if(NOT VK_SDK_PATH)
	color_message(FATAL_ERROR "\nVulkan SDK location is not set!\nSet the VK_SDK_PATH cmake or environmental variable to define the SDK's location!\nGet the SDK from: https://www.lunarg.com/vulkan-sdk/\n")
endif()

if(NOT EXISTS ${VK_SDK_PATH})
	color_message(FATAL_ERROR "\nVK_SDK_PATH is set to \"${VK_SDK_PATH}\" but the directory does not exists!\n")
endif()

# Find library
if(MINGW)
	find_file(VULKAN_LIBRARY NAMES "vulkan-1.dll" PATHS "$ENV{WINDIR}/System32/")
	if(NOT VULKAN_LIBRARY)
		color_message(FATAL_ERROR "vulkan-1.dll not found!\n")
	endif()
elseif(MSVC)
	set(VULKAN_LIBRARY "${VK_SDK_PATH}/Lib/vulkan-1.lib")
endif()

# Create target
add_library(${VULKAN_TARGET} INTERFACE)
target_link_libraries(${VULKAN_TARGET} INTERFACE ${VULKAN_LIBRARY})
target_include_directories(${VULKAN_TARGET} INTERFACE "${VK_SDK_PATH}/include")

color_message("%greenOK (SDK Path: ${VK_SDK_PATH})\n")
