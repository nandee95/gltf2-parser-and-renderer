#version 440
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNorm;
layout(location = 2) in vec2 inUv;

layout(location = 0) out vec3 outNorm;
layout(location = 1) out vec3 outPosition;
layout(location = 2) out vec2 outUv;

layout(set=0,binding = 0) uniform SceneProperties {
    mat4 projView;
    vec3 camPos;
    uint shading;
    float scale;
} props;

void main() {
    outPosition = inPosition*props.scale;
    gl_Position = props.projView * vec4(outPosition,1.0);
    outNorm = inNorm;
    outUv = inUv;
}