#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec3 inCubeUv;

layout(set=0,binding = 1) uniform samplerCube cubeMap;

void main() {
    vec4 textureSample = texture(cubeMap, inCubeUv);
    outColor = textureSample;
}