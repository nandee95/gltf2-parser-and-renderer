#version 440
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 0) out vec3 outCubeUv;

layout(set=0,binding = 0) uniform SceneProperties {
    mat4 projView;
    vec3 camPos;
} props;

void main() {
    gl_Position = props.projView * vec4(props.camPos + inPosition *10000,1.0);
    outCubeUv = inPosition;
}