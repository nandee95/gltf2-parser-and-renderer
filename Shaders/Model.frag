#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec3 inNorm;
layout(location = 1) in vec3 inPosition;
layout(location = 2) in vec2 inUv;

layout(set=0,binding = 0) uniform SceneProperties {
    mat4 projView;
    vec3 camPos;
    uint shading;
} props;

layout(set=0,binding = 1) uniform samplerCube cubeMap;
layout(set=0,binding = 2) uniform sampler2D descTexture;

vec3 lightDirection = vec3(0.707,0.707,0);

void main() {
    vec3 I = normalize(inPosition - props.camPos);
    vec3 R = reflect(I, normalize(inNorm));
    vec4 reflectColor =  vec4(texture(cubeMap, R).rgb, 1.0);

    float light =(dot(lightDirection,inNorm)+1)/2;
    switch(props.shading)
    {
        case 0: 
            outColor = texture(descTexture,inUv);
            break;
        case 1: 
            outColor = vec4(mix(vec3(0.07,0.3,0),vec3(0.1,0.85,0),light),1);
            break;
        default:
            outColor = vec4(reflectColor.rgb,1);
    }
}