#version 440
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;

layout(set=0,binding = 0) uniform SceneProperties {
    mat4 projView;
} props;

void main() {
    gl_Position = props.projView * vec4(inPosition, 1.0);
}