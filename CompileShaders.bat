@echo off
glslc ./Shaders/Grid.frag -o ./Shaders/Compiled/Grid.frag.spv
glslc ./Shaders/Grid.vert -o ./Shaders/Compiled/Grid.vert.spv
glslc ./Shaders/Sky.frag -o ./Shaders/Compiled/Sky.frag.spv
glslc ./Shaders/Sky.vert -o ./Shaders/Compiled/Sky.vert.spv
glslc ./Shaders/Model.frag -o ./Shaders/Compiled/Model.frag.spv
glslc ./Shaders/Model.vert -o ./Shaders/Compiled/Model.vert.spv
echo Done!
pause>nul